$(document).ready(function(){
	var baselink = $("#baselink").val();
		id_kerja = $("#id_kerja").val();
		status_kerja = $("#status_kerja").val();
	CKEDITOR.replace("deskripsi");

	$("#tanggal").datepicker({
	   autoclose: true
	})	
	$("#btnAddAbsen").click(function(){
		$("#modalTitle").html("Tambah Data Issue");
		$("#id_sub_detail_pekerjaan").val(7)
		$("#manipulateModal").modal("show");
	})
	$("#btnAddMom").click(function(){
		$("#modalTitle").html("Tambah Data Issue");
		$("#id_sub_detail_pekerjaan").val(8)
		$("#manipulateModal").modal("show");
	})
	list();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'maintenance/list/',
			data:{"id_kerja":id_kerja},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listViewMom").html("");
				$("#listViewAbsen").html("");
				$.each(data,function(i,item){
					parseList(item,i+1);
				});
				$("#tableAbsen").DataTable({autoWidth:false,scrollX:true});
				$("#tableMom").DataTable({autoWidth:false,scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});			
	}
	function parseList(data,i){
		console.log(data);
		var action = "";
		if(status_kerja == "on progress"){
			action = '<button id="itemEdit" class="btn btn-warning" data-id="'+data.id_detail_kerja+'" data-sub="'+data.id_sub_detail_pekerjaan+'" type="button" style="margin-left: 5px; margin-right: 5px;"><i class="fa fa-pencil"></i></button>'+
					'<button id="itemDelete" class="btn btn-danger" data-id="'+data.id_detail_kerja+'" type="button" style="margin-left: 5px; margin-right: 5px;"><i class="fa fa-trash"></i></button>'+
					'<button id="itemDetail" class="btn btn-info" data-id="'+data.id_detail_kerja+'" type="button" style="margin-left: 5px; margin-right: 5px;"><i class="fa fa-eye"></i></button>';
					
		} else {
				action = '<button id="itemDetail" class="btn btn-info" type="button" data-id="'+data.id_detail_kerja+'" style="margin-left: 5px; margin-right: 5px;"><i class="fa fa-eye"></i></button>';
					
		}
		var text = "<tr><td>"+data.nama+"</td>"+
				   "<td>"+data.date+"</td>"+
				   "<td>"+data.note+"</td>"+
				   "<td style='text-align:center'>"+action+"</td></tr>";
		if(data.id_sub_detail_pekerjaan == "7"){
			$("#listViewAbsen").append(text);
		} else {
			$("#listViewMom").append(text);
		}							
	}
	$("#listViewAbsen").on("click","#itemEdit",function(){
		var id_detail_kerja = $(this).attr("data-id");
		detail(id_detail_kerja);
		$("#modalTitle").html("Edit Data Issue");
		$("#id_sub_detail_pekerjaan").val(7)
		$("#id_detail_kerja").val(id_detail_kerja);
	});
	$("#listViewMom").on("click","#itemEdit",function(){
		var id_detail_kerja = $(this).attr("data-id");
		detail(id_detail_kerja);
		$("#modalTitle").html("Edit Data Fixing");
		$("#id_sub_detail_pekerjaan").val(8)
		$("#id_detail_kerja").val(id_detail_kerja);
	});	
	function resetForm(){
		$("input[type=text]").val('');
		CKEDITOR.instances.deskripsi.setData("");
		$("#manipulateForm").attr("action",baselink+"maintenance/add");		
	}
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetForm();
	});
	$("#listViewAbsen").on("click","#itemDetail",function(){
		var id_detail_kerja = $(this).attr("data-id");
		detail(id_detail_kerja,1);
	});
	$("#listViewMom").on("click","#itemDetail",function(){
		var id_detail_kerja = $(this).attr("data-id");
		detail(id_detail_kerja,1);
	});	
	function detail(id_detail_kerja,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'maintenance/detail/',
			data:{"id_detail_kerja":id_detail_kerja},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if(type == 0){
					parseEdit(data[0]);
				} else {
					parseDetail(data[0]);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseEdit(data){
		$("#tanggal").val(data.date);
		CKEDITOR.instances.deskripsi.setData(data.note);
		$("#nama").val(data.nama);
		$("#manipulateForm").prop("action",baselink+"maintenance/edit");
		$("#manipulateModal").modal("show");
	}
	function parseDetail(data){
		$("#detailNama").val(data.nama);
		$("#detailTanggal").val(data.date);
		$("#detailDeskripsi").html(data.note);
		$("#detailCreateAt").val(data.created_at);
		if(data.updated_at != null){
			$("#detailUpdateAt").val(data.updated_at);
		}
		$("#detailModal").modal("show");
	}
	$("#listViewAbsen").on("click","#itemDelete",function(){
		var id_detail_kerja = $(this).attr("data-id");
		$("#btn_modal").attr("data-id",id_detail_kerja);
		$("#modal_delete").modal("show");
	});
	$("#listViewMom").on("click","#itemDelete",function(){
		var id_detail_kerja = $(this).attr("data-id");
		$("#btn_modal").attr("data-id",id_detail_kerja);
		$("#modal_delete").modal("show");
	});
	$("#btn_modal").click(function(){
		var id_detail_kerja = $(this).attr("data-id");
		$.ajax({
			type: "POST",
			url: baselink+'maintenance/delete/',
			data:{"id_detail_kerja":id_detail_kerja},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if(data.status == "success"){
					alert("Berhasil menghapus data")
					$("#modal_delete").modal("hide");
					$("#tableAbsen").DataTable().destroy();								
					$("#tableMom").DataTable().destroy();
					list();
				} else {
					alert("Gagal menghapus data")
				}
				
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	$("#listViewAbsen").on("click","#itemDownload",function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');	
		
	});
	$("#listViewMom").on("click","#itemDownload",function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');
	});	
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		CKEDITOR.instances.deskripsi.updateElement();
		$.ajax({
							type: "POST",
							url: $(this).prop("action"),
							data: new FormData( this ),
					      	processData: false,
					      	contentType: false,
							success: function(response){
								var data = jQuery.parseJSON(response);
								if (data.status == "success"){
									alert("Data maintenance berhasil disimpan");
									$("#manipulateModal").modal("hide");	
									$("#tableAbsen").DataTable().destroy();								
									$("#tableMom").DataTable().destroy();							
									list();
								}else{
									alert("Gagal menyimpan data maintenance");
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
						        console.log(xhr.status);
						        console.log(xhr.responseText);
						        console.log(thrownError);
						    }
						});	
		
	});
	$("#tabBar1").click(function(){
        $("#tableMom").DataTable().destroy();
        $("#tableAbsensi").DataTable().destroy();
        $("#tableAbsensi").DataTable({autoWidth:false,scrollX:true});
        $(".dataTables_scrollHeadInner").attr("style",'box-sizing: content-box;width: 100%;padding-right: 0px;');
        $(".dataTable").attr("style","margin-left: 0px;");
    });
    $("#tabBar2").click(function(){
        $("#tableAbsensi").DataTable().destroy();
        $("#tableMom").DataTable().destroy();
        $("#tableMom").DataTable({autoWidth:false,scrollX:true});;
        $(".dataTables_scrollHeadInner").attr("style",'box-sizing: content-box;width: 100%;padding-right: 0px;');
        $(".dataTable").attr("style","margin-left: 0px;");
    });	
});