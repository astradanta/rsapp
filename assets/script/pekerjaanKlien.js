$(document).ready(function(){
	var baselink = $("#baselink").val();
	var idPekerjaanKlien = $("#idPekerjaanKlien").val();
	displayList();
	listPekerjaanKlien();
	function displayList(){

		$.ajax({
			type: "POST",
			url: baselink+'pekerjaanKlien/list',
			data:{"baselink":baselink,"id":idPekerjaanKlien},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function listPekerjaanKlien(){
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaanKlien/listPekerjaanKlien',
			data:{"baselink":baselink,"id":idPekerjaanKlien},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listPekerjaan").html('');
				$.each(data,function(i,item){
					parseListPekerjaanKlien(i,item);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function parseListPekerjaanKlien(i,item){
		console.log(item);
		var text = '<tr>'+
					'<td>'+(parseInt(i)+1)+'</td>'+
					'<td>'+item.nama_kerja+'</td>'+
					'</tr>';
		$("#listPekerjaan").append(text);
	}
	function parseList(data){
		var totalKerja = parseInt(data.total_kerja);
		var totalDikerjakan = parseInt(data.total_dikerjakan);
		var persentase = Math.round((totalDikerjakan/totalKerja)*100);
		$("#listItemTemplate").find($("#itemContainer").attr("data-id",data.id_kerja));
		$("#listItemTemplate").find($("#itemContainer").attr("data-pekerjaan",data.id_pekerjaan));
		$("#listItemTemplate").find($("#itemLeftIcon").attr("data-id",data.id_kerja));
		$("#listItemTemplate").find($("#itemLeftIcon").attr("data-pekerjaan",data.id_pekerjaan));		
		$("#listItemTemplate").find($("#itemlistDeadline").html(data.deadline));
		$("#listItemTemplate").find($("#itemlistDeadline").attr("data-id",data.id_kerja));
		$("#listItemTemplate").find($("#itemlistDeadline").attr("data-pekerjaan",data.id_pekerjaan));
		$("#listItemTemplate").find($("#jenisPekerjaan").html(data.nama_pekerjaan));
		$("#listItemTemplate").find($("#namaPekerjaan").html(data.nama_kerja));
		$("#listView").append($("#listItemTemplate").html());
	}
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetModal();
	});		
	$('#listView').on('click','#itemContainer',function(e) { 
			var id_pekerjaan = $(this).attr("data-pekerjaan");
			var id_kerja = $(this).attr("data-id");
			var url = baselink;
			if(id_pekerjaan == 1){
				url +="assessment/"+id_kerja;
			} else {
				url +="kom_product/"+id_kerja;
			}
			window.location.href = url;				
	});	
	$('#listView').on('click','#itemlistDeadline',function(e) { 
			var id_pekerjaan = $(this).attr("data-pekerjaan");
			var id_kerja = $(this).attr("data-id");
			var url = baselink;
			if(id_pekerjaan == 1){
				url +="assessment/"+id_kerja;
			} else {
				url +="kom_product/"+id_kerja;
			}
			window.location.href = url;	
	});	
	$('#listView').on('click','#itemLeftIcon',function(e) { 
			var id_pekerjaan = $(this).attr("data-pekerjaan");
			var id_kerja = $(this).attr("data-id");
			var url = baselink;
			if(id_pekerjaan == 1){
				url +="assessment/"+id_kerja;
			} else {
				url +="kom_product/"+id_kerja;
			}
			window.location.href = url;		
	});		
});