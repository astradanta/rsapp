$(document).ready(function(){
	var baselink = $("#baselink").val();
	list();
	selectInvoice();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'receipt/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);

				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableInvoice").DataTable({scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){
		var text = 	'<tr>'+
						'<td>'+data.nama+'</td>'+
						'<td>'+data.no_invoice+'</td>'+
						'<td>'+data.deskripsi+'</td>'+
						'<td>'+data.created_at+'</td>'+
						'<td>'+data.updated_at+'</td>'+						
						'<td style="text-align:center;">'+
							'<button data-id="'+data.id_receipt+'" class="btn btn-warning " id="editItem" type="button"><i class="fa fa-pencil"></i></button>'+
							'<button data-id="'+data.id_receipt+'" class="btn btn-info " id="viewItem" data-url="'+data.file_receipt+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-eye"></i></button>'+
							'<button data-id="'+data.id_receipt+'" class="btn btn-danger " id="deleteItem" type="button"><i class="fa fa-trash"></i></button>'+
						'</td>'+
					'</tr>';
		$("#listView").append(text);
	}
	function selectInvoice(){
		$.ajax({
			type: "POST",
			url: baselink+'receipt/listInvoice',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#selectPicker").html('');
				$("#selectPicker").append('<option value="0">Pilih invoice</option>')
				$.each(data,function(i,item){
					$("#selectPicker").append('<option value="'+item.id_invoice+'">'+item.no_invoice+'</option>')
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		 if ($("#selectPicker").val()==0){
			alert("Anda harus memilih klien");
		} else {
			$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data receipt berhasil disimpan");
						$("#manipulateModal").modal("hide");
						$("#tableInvoice").DataTable().destroy();
						list();
					}else{
						alert("Gagal menyimpan data invoice");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});			
		}
		
	});
	function resetModal(){
		$('input[type="text"]').val('');
		$('textarea').val('');
		$("#selectPicker").val($("#selectPicker option:first").val());
		$("#manipulateForm").prop('action',baselink+'receipt/add');
		$("#modalTitle").html("Tambah Data Receipt");
	}
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetModal();
	  	selectInvoice();
	});	
	$("#listView").on('click','#editItem',function(){
		var id = $(this).attr('data-id');
		getDetail(id);
	});	
	function getDetail(id){
		$.ajax({
			type: "POST",
			url: baselink+'receipt/detail',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				parseDetail(data[0]);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseDetail(data){
		$("#selectPicker option[value=0]").html(data.no_invoice);
		$("#selectPicker option[value=0]").val(data.id_invoice);
		$("#inputDeskripsi").val(data.deskripsi);
		$("#inputUrl").val(data.file_receipt);		
		$("#idDetail").val(data.id_receipt);
		$("#manipulateModal").modal("show");
		$("#manipulateForm").prop('action',baselink+'receipt/edit');
		$("#modalTitle").html("Ubah Data Receipt");	
	}
	$("#listView").on('click','#viewItem',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});	
	$("#listView").on('click','#deleteItem',function(){
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'receipt/delete',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Data Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#tableInvoice").DataTable().destroy();
					list();
					selectInvoice();
				} else {
					alert("Gagal menghapus data")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});								
});