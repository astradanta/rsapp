$(document).ready(function(){
	var baselink = $("#baselink").val();
		id_client = $("#idClient").val();

	CKEDITOR.replace('catatan');
	list();
	selectPicker();
	listSlip();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'penggajian/list',
			data:{'id_client':id_client},
			cache: false,
			success: function(response){

                var data = jQuery.parseJSON(response);
                $("#listView").html("");
                $.each(data,function(i,item){
                    parseList(item);
                });
                $("#tableAbsensi").DataTable({autoWidth:false,scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
    function parseList(data){
    	var status = "Diverifikasi";
    	if(data.pengerjaan_status == 0){
    		status = "Tidak Diverifikasi";
    	}
        var text =  '<tr>'+
                        '<td>'+data.tanggal+'</td>'+
                        '<td>'+data.periode+'</td>'+
                        '<td>'+data.note+'</td>'+
                         '<td>'+status+'</td>';
        if(data.pengerjaan_status == 1){
            $("#lockedAction").find("#itemDownload").attr("data-url",data.upload_file);
            $("#lockedAction").find("#itemEdit").attr("data-id",data.id_kerja);
            text += "<td style='text-align:center'>"+$("#lockedAction").html()+"</td></tr>";
        } else {
            $("#openAction").find("#itemEdit").attr("data-id",data.id_kerja);
            $("#lockedAction").find("#itemDownload").attr("data-url",data.upload_file);
            text += "<td style='text-align:center'>"+$("#openAction").html()+"</td></tr>";
        }
        $("#listView").append(text);
    }
    $("#listView").on('click','#itemDownload',function(){
        var url = $(this).attr('data-url');
        window.open(url, 'name');       
    });	
    $("#listView").on('click','#itemEdit',function(){
    	var id = $(this).attr('data-id');
    	$("#idDetail").val(id);
    	$("#firstRadio").prop("checked",false);
    	$("#secondRadio").prop("checked",false);
		$("#statusAbsensiModal").modal("show");
    });
	$("#statusAbsensiForm").submit(function(e){
			e.preventDefault();
			if(($("#firstRadio").prop('checked')==false) && ($("#secondRadio").prop('checked')==false)){
				alert("pilih salah satu status");
			}  else {
					$.ajax({
					type: "POST",
					url: $(this).prop("action"),
					data: new FormData( this ),
			      	processData: false,
			      	contentType: false,
					success: function(response){
						var data = jQuery.parseJSON(response)
						if(data.status == "success"){
							alert("Status Berhasil Diganti");
							$("#tableAbsensi").DataTable().destroy();
							list();
							selectPicker();
							$("#statusAbsensiModal").modal('hide');
						} else {
							alert("Status Ganti Diganti")
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
				        console.log(xhr.status);
				        console.log(xhr.responseText);
				        console.log(thrownError);
				    }
				});			
			}
			
	});    
	function selectPicker(){
		$.ajax({
			type: "POST",
			url: baselink+'penggajian/picker',
			data:{'id_client':id_client},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#selectPicker").html('');
				$("#selectPicker").append('<option value="0">Pilih Periode</option>')
				$.each(data,function(i,item){
					$("#selectPicker").append('<option value="'+item.id_kerja+'">'+item.periode+'</option>')
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});			
	}
	function listSlip(){
		$.ajax({
			type: "POST",
			url: baselink+'penggajian/listSlip',
			data:{'id_client':id_client},
			cache: false,
			success: function(response){
                var data = jQuery.parseJSON(response);
                $("#slipView").html("");
                $.each(data,function(i,item){
                    parseListSlip(item);
                });
                $("#tableSlip").DataTable({autoWidth:false,scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
 function parseListSlip(data){
    	var status = "Diverifikasi";
    	if(data.pengerjaan_status == 0){
    		status = "Tidak Diverifikasi";
    	}
        var text =  '<tr>'+
                        '<td>'+data.tanggal+'</td>'+
                        '<td>'+data.periode+'</td>'+
                        '<td>'+data.note+'</td>';
        $("#slipAction").find("#itemEdit").attr("data-id",data.id_kerja);
        $("#slipAction").find("#itemDelete").attr("data-id",data.id_kerja);
        $("#slipAction").find("#itemFinish").attr("data-id",data.id_kerja);
        $("#slipAction").find("#itemDownload").attr("data-url",data.upload_file);
         text += '<td style="text-align:center">'+$("#slipAction").html()+'</td></tr>';

        $("#slipView").append(text);
    }
    $("#tabBar1").click(function(){
    	$("#tableSlip").DataTable().destroy();
    	$("#tableAbsensi").DataTable().destroy();
    	$("#tableAbsensi").DataTable({autoWidth:false,scrollX:true});
    	$(".dataTables_scrollHeadInner").attr("style",'box-sizing: content-box;width: 100%;padding-right: 0px;');
    	$(".dataTable").attr("style","margin-left: 0px;");
    });
    $("#tabBar2").click(function(){
    	$("#tableAbsensi").DataTable().destroy();
    	$("#tableSlip").DataTable().destroy();
    	$("#tableSlip").DataTable({autoWidth:false,scrollX:true});;
    	$(".dataTables_scrollHeadInner").attr("style",'box-sizing: content-box;width: 100%;padding-right: 0px;');
    	$(".dataTable").attr("style","margin-left: 0px;");
    });
	$("#slipView").on('click','#itemDelete',function(e){
        e.preventDefault()
        var id = $(this).attr('data-id');
        $("#modal_delete").modal("show");
        $("#btn_modal").attr('data-id',id);
    });  
    $("#btn_modal").click(function(){
        var id = $(this).attr("data-id")
        $.ajax({
            type: "POST",
            url: baselink+'penggajian/delete',
            data:{"baselink":baselink,"id":id},
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                if (data.status == "success"){
                    alert("Data Berhasil dihapus");
                    $("#modal_delete").modal("hide");
                    $("#tableSlip").DataTable().destroy();
                    listSlip()
                } else {
                    alert("Gagal menghapus data")
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }); 
    $("#manipulateModal").on("hidden.bs.modal", function () {
        resetModal();
    });
    function resetModal(){
        $("input[type=text]").val("");
        CKEDITOR.instances.catatan.setData("");
        $("#modalTitle").html("Form Tambah Data Slip Gaji");
        $("#manipulateForm").prop("action",baselink+"penggajian/add");
        selectPicker();
    } 
    $("#slipView").on('click','#itemEdit',function(){
        var id = $(this).attr('data-id');
        detail(id);
    });
    function detail(id){
        $.ajax({
            type: "POST",
            url: baselink+'penggajian/detail',
            data:{"id_kerja":id,"id_client":id_client},
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                parseDetail(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
    function parseDetail(data){
        console.log(data);
		$("#selectPicker option[value=0]").html(data[0].periode);
		$("#selectPicker option[value=0]").val(data[0].id_kerja);
        $("#url").val(data[0].upload_file);
        $("#idDetailSlip").val(data[0].id_detail_kerja);
        CKEDITOR.instances.catatan.setData(data[0].note);
        $("#modalTitle").html("Form Ubah Data Slip Gaji");
        $("#manipulateForm").prop("action",baselink+"penggajian/edit");
        $("#manipulateModal").modal("show");
    }
    $("#manipulateForm").submit(function(e){
        e.preventDefault();
        CKEDITOR.instances.catatan.updateElement();
            $.ajax({
                type: "POST",
                url: $(this).prop("action"),
                data: new FormData( this ),
                processData: false,
                contentType: false,
                success: function(response){
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if (data.status == "success"){
                        alert("Data absensi berhasil disimpan");
                        $("#manipulateModal").modal("hide");
                        $("#tableSlip").DataTable().destroy();
                        listSlip();
                    }else{
                        alert("Gagal menyimpan data absensi");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });         
        
    });
   $("#slipView").on('click','#itemDownload',function(){
        var url = $(this).attr('data-url');
        window.open(url, 'name');       
    });
   $("#slipView").on('click','#itemFinish',function(){
        var id = $(this).attr('data-id');
        finishIt(id)      ;
    });
    function finishIt(id)   {
         $.ajax({
            type: "POST",
            url: baselink+'penggajian/finish',
            data:{"id_kerja":id},
            cache: false,
            success: function(response){
            	var data = jQuery.parseJSON(response);
            	if(data.status == "success"){
            		alert("Berhasil mengubah status menjadi selesai");
            		$("#tableAbsensi").DataTable().destroy();
            		list();
            		$("#tableSlip").DataTable().destroy();
            		listSlip();
            	} else {
            		alert("Gagal mengubah status");
            	}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });   	
    }
});