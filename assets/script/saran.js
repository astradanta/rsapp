$(document).ready(function(){
	var baselink = $("#baselink").val();
	list();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'saran/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);

				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableInvoice").DataTable({scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){
		var text = 	'<tr>'+
						'<td>'+data.nama_client+'</td>'+
						'<td>'+data.create_at+'</td>'+
						'<td>'+data.content+'</td>'+
						'<td>'+data.update_at+'</td>'+
						'<td style="text-align:center;">'+							
							'<button data-id="'+data.id_saran+'" class="btn btn-danger " id="deleteItem" type="button"><i class="fa fa-trash"></i>&nbsp;Delete</button>'+
						'</td>'+
					'</tr>';
		$("#listView").append(text);
	}
	$("#listView").on('click','#deleteItem',function(){
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'saran/delete',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Data Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#tableInvoice").DataTable().destroy();
					list()
				} else {
					alert("Gagal menghapus data")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});			
});