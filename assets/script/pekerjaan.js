$(document).ready(function(){
	var baselink = $("#baselink").val();
		id_client = $("#idPekerjaanKlien").val();
		jenis = "-";
	list();

	$("#mulai").datepicker({
	   autoclose: true
	})
	$("#selesai").datepicker({
	   autoclose: true
	})
	CKEDITOR.replace('note');
	resetForm();	
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaan/list/',
			data:{"id_client":id_client},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html("");
				$.each(data,function(i,item){
					parseList(item,i+1);
				});
				$("#tablePekerjaan").DataTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});			
	}
	function parseList(item,i){
		var text = "<tr><td>"+i+"</td>"+
					"<td>"+item.nama_kerja+"</td>"+
					"<td>"+item.jenis+"</td>"+
					"<td>"+item.pic+"</td>"+
					"<td>"+item.start_date+"</td>"+
					"<td>"+item.deadline+"</td>"+
					"<td>"+item.status+"</td>";
		var action = '<button class="btn btn-info" id="itemDetail" style="margin:5px;" data-id="'+item.id_kerja+'"><i class="fa fa-eye"></i></button>'+
					'<button class="btn btn-warning" id="itemEdit" style="margin:5px;" data-id="'+item.id_kerja+'"><i class="fa fa-pencil"></i></button>'+
					'<button class="btn btn-danger" id="itemDelete" style="margin:5px;" data-id="'+item.id_kerja+'"><i class="fa fa-trash"></i></button>'+
					'<button class="btn btn-warning" id="itemChange" style="margin:5px;" data-id="'+item.id_kerja+'" data-status="'+item.status+'"><i class="fa fa-exchange"></i></button>';
		text +="<td style='text-align:center;'>"+action+"</td></tr>";
		$("#listView").append(text);
	}
	function resetForm(){
		$("input[type=text]").val('');
		$("#radio1").prop("checked",true);
		CKEDITOR.instances.note.setData("");
		$("#modalTitle").html("Tambah Data Pekerjaan");
		$("#selectPIC option[value='0']").prop("selected",true);
		$("#manipulateForm").attr("action",baselink+"pekerjaan/add");		
	}
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetForm();
	});
	$("#listView").on('click','#itemEdit',function(){
		var id = $(this).attr("data-id");
		detail(id);
	})
	$("#listView").on('click','#itemDetail',function(){
		var id = $(this).attr("data-id");
		detail(id,1);
	})

	function detail(id,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaan/detail/',
			data:{"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if(type == 0){
					parseEdit(data[0]);
				} else {
					parseDetail(data[0]);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseEdit(data){
		$("#id_kerja").val(data.id_kerja);
		$("#id_daftar_pekerjaan_client").val(data.id_daftar_pekerjaan_client);
		$("#nama_kerja").val(data.nama_kerja);
		$("#selectPIC option[value="+data.id_user+"]").prop("selected",true);
		$("#radio"+data.id_pekerjaan).prop("checked",true);
		$("#mulai").val(data.start_date);
		$("#selesai").val(data.deadline);
		CKEDITOR.instances.note.setData(data.deskripsi);		
		$("#modalTitle").html("Ubah Data Pekerjaan");
		$("#manipulateForm").attr("action",baselink+"pekerjaan/edit");
		$("#manipulateModal").modal("show");
	}
	function parseDetail(data){
		console.log(data);
		$("#detailPekerjaan").val(data.nama_kerja);
		$("#detailJenis").val(data.jenis);
		$("#detailPIC").val(data.pic);
		$("#detailMulai").val(data.start_date);
		$("#detailSelesai").val(data.deadline);
		$("#detailSyarat").html(data.deskripsi);
		$("#detailStatus").val(data.status);
		$("#detailCreateAt").val(data.created_at);
		if(data.updated_at != null){
			$("#detailUpdateAt").val(data.updated_at);
		}
		$("#detailModal").modal("show");
	}
	$("#listView").on('click','#itemDelete',function(){
		var id = $(this).attr('data-id');
		$("#btn_modal").attr('data-id',id);
		$("#modal_delete").modal("show");
	})
	$("#btn_modal").click(function(){
		var id = $(this).attr('data-id');
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaan/delete/',
			data:{"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response)
				if(data.status == "success"){
					$("#modal_delete").modal("hide");
					$("#tablePekerjaan").DataTable().destroy();
					list();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	});
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		CKEDITOR.instances.note.updateElement();
		if($("#selectPIC").val() == 0 ){
			alert("anda harus memilih pic");
		} else 
			{
				$.ajax({
							type: "POST",
							url: $(this).prop("action"),
							data: new FormData( this ),
					      	processData: false,
					      	contentType: false,
							success: function(response){
								var data = jQuery.parseJSON(response);
								if (data.status == "success"){
									alert("Data pekerjaan berhasil disimpan");
									$("#manipulateModal").modal("hide");
									$("#tablePekerjaan").DataTable().destroy();
									list();
								}else{
									alert("Gagal menyimpan data pekerjaan");
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
						        console.log(xhr.status);
						        console.log(xhr.responseText);
						        console.log(thrownError);
						    }
						});	
		}
		
	});	
	$("#listView").on('click','#itemChange',function(){
		var status = $(this).attr("data-status");
			id = $(this).attr("data-id");
		if(status == "on progress"){
			$("#radio_onprogress").prop("checked",true);
		}else{
			$("#radio_"+status).prop("checked",true);
		}
		$("#id_kerja_change").val(id);
		$("#modal_change").modal("show");
	})
	$("#changeForm").submit(function(e){
		e.preventDefault();
		
		$.ajax({
							type: "POST",
							url: $(this).prop("action"),
							data: new FormData( this ),
					      	processData: false,
					      	contentType: false,
							success: function(response){
								var data = jQuery.parseJSON(response);
								if (data.status == "success"){
									alert("Status pekerjaan berhasil diganti");
									$("#modal_change").modal("hide");
									$("#tablePekerjaan").DataTable().destroy();
									list();
								}else{
									alert("Gagal mengganti status pekerjaan");
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
						        console.log(xhr.status);
						        console.log(xhr.responseText);
						        console.log(thrownError);
						    }
						});
		
	});	
	$("#selectJenis").change(function(){
		jenis = $(this).val();
		$("#tablePekerjaan").DataTable().draw();
	});
	$.fn.dataTableExt.afnFiltering.push(
	  function(oSettings, aData, iDataIndex) {
	    if(jenis != "-"){
	    	if(jenis != aData[2]){
	    		return false;
	    	}
	    }

	    return true;
	  }
	);
});	
