$(document).ready(function(){
	var baselink = $("#baselink").val();
		id_kerja = $("#id_kerja").val();
		status_kerja = $("#status_kerja").val();
	CKEDITOR.replace("deskripsi");

	$("#tanggal").datepicker({
	   autoclose: true
	})
	$("#deadline").datepicker({
	   autoclose: true
	})		
	$("#btnAddAbsen").click(function(){
		
		$("#id_sub_detail_pekerjaan").val(3)
		$("#manipulateModal").modal("show");
	})
	list();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'systemRunning/list/',
			data:{"id_kerja":id_kerja},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html("");
				$.each(data,function(i,item){
					parseList(item,i+1);
				});
				$("#tableDevelopt").DataTable({
					scrollX:true
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});			
	}
	function parseList(data,i){
		console.log(data);
		var action = "<button class='btn btn-warning' id='itemEdit' data-id='"+data.id_detail_kerja+"' style='margin:5px;'><i class='fa fa-pencil'></i></button>"+
					"<button class='btn btn-danger' id='itemDelete' data-id='"+data.id_detail_kerja+"' style='margin:5px;'><i class='fa fa-trash'></i></button>"+					
					"<button class='btn btn-info' id='itemDetail' data-id='"+data.id_detail_kerja+"' style='margin:5px'><i class='fa fa-eye'></i></button>";
		if(status_kerja == "selesai"){
			action = "<button class='btn btn-info' id='itemDetail' data-id='"+data.id_detail_kerja+"' style='margin:5px'><i class='fa fa-eye'></i></button>";
		}
		var text = "<tr><td>"+i+"</td>"	+
					"<td>"+data.nama+"</td>"+					
					"<td>"+data.date+"</td>"+
					"<td>"+data.note+"</td>";	
					  text +="<td style='text-align:center;'>"+action+"</td></tr>";

		$("#listView").append(text);										
	}
	$("#listView").on("click","#itemEdit",function(){
		var id_detail_kerja = $(this).attr("data-id");
		detail(id_detail_kerja);
		$("#modalTitle").html("Tambah Data List Development");
		$("#id_sub_detail_pekerjaan").val(1)
		$("#id_detail_kerja").val(id_detail_kerja);
	});
	$("#listView").on("click","#itemDetail",function(){
		var id_detail_kerja = $(this).attr("data-id");
		detail(id_detail_kerja,1);
	});	
	function detail(id_detail_kerja,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'system_product//detail/',
			data:{"id_detail_kerja":id_detail_kerja},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if(type == 0){
					parseEdit(data[0]);
				} else {
					parseDetail(data[0]);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseEdit(data){
		$("#nama").val(data.nama);
		$("#tanggal").val(data.date);
		CKEDITOR.instances.deskripsi.setData(data.note);
		$("#upload_file").val(data.upload_file);
		$("#absen").val(data.document_1);
		$("#mom").val(data.document_2);
		$("#manipulateForm").prop("action",baselink+"system_product//edit");
		$("#manipulateModal").modal("show");
	}
	function parseDetail(data){
		$("#detailNama").val(data.nama);
		$("#detailTanggal").val(data.date);
		$("#detailDeskripsi").html(data.note);
		$("#detailCreateAt").val(data.created_at);
		
		if(data.updated_at != null){
			$("#detailUpdateAt").val(data.updated_at);
		}
		$("#detailModal").modal("show");
	}
	$("#listView").on("click","#itemDelete",function(){
		var id_detail_kerja = $(this).attr("data-id");
		$("#btn_modal").attr("data-id",id_detail_kerja);
		$("#modal_delete").modal("show");
	});
	$("#btn_modal").click(function(){
		var id_detail_kerja = $(this).attr("data-id");
		$.ajax({
			type: "POST",
			url: baselink+'systemRunning/delete/',
			data:{"id_detail_kerja":id_detail_kerja},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if(data.status == "success"){
					alert("Berhasil menghapus data")
					$("#modal_delete").modal("hide");
					$("#tableDevelopt").DataTable().destroy();
					list();
				} else {
					alert("Gagal menghapus data")
				}
				
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	$("#listView").on("click","#itemChange",function(){
		var id = $(this).attr("data-id");
			status = $(this).attr("data-status");
		if(status == "0" ){
			$("#radio_onprogress").prop("checked",true);
		} else {
			$("#radio_selesai").prop("checked",true);
		}
		$("#id_detail_kerja_change").val(id);
		$("#modal_change").modal("show");
		
	});
	function resetForm(){
		$("input[type=text]").val('');
		CKEDITOR.instances.deskripsi.setData("");
		$("#manipulateForm").attr("action",baselink+"systemRunning/add");
		$("#modalTitle").html("Tambah Data List Instalasi");		
	}
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetForm();
	});
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		CKEDITOR.instances.deskripsi.updateElement();
		$.ajax({
							type: "POST",
							url: $(this).prop("action"),
							data: new FormData( this ),
					      	processData: false,
					      	contentType: false,
							success: function(response){
								var data = jQuery.parseJSON(response);
								if (data.status == "success"){
									alert("Data System Running berhasil disimpan");
									$("#manipulateModal").modal("hide");
									$("#tableDevelopt").DataTable().destroy();								
									list();
								}else{
									alert("Gagal menyimpan data System Running");
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
						        console.log(xhr.status);
						        console.log(xhr.responseText);
						        console.log(thrownError);
						    }
						});	
		
	});
	
});