$(document).ready(function() {
    function e() {
        $.ajax({
            type: "POST",
            url: b + "user/listUser",
            data: {
                baselink: b
            },
            cache: !1,
            success: function(a) {
                a = jQuery.parseJSON(a);
                var d = 1;
                $("#listView").html("");
                $.each(a, function(g, a) {
                    var c = d;
                    $("#actionItemTemplate").find("#editList").attr("data-id", a.id_user);
                    $("#actionItemTemplate").find("#deleteList").attr("data-id", a.id_user);
                    $("#actionItemTemplate").find("#detailList").attr("data-id", a.id_user);
                    c = "<tr><td>" + c + "</td><td>" + a.nama + "</td><td>" + a.email + "</td><td>" + a.jenis_user +
                        '</td><td style="display:flex">' + $("#actionItemTemplate").html() + "</td></tr>";
                    $("#listView").append(c);
                    d++
                });
                $("#userTable").DataTable({
                    scrollX: !0
                })
            },
            error: function(a, d, g) {
                console.log(a.status);
                console.log(a.responseText);
                console.log(g)
            }
        })
    }

    function h() {
        $('input[type="text"]').val("");
        $('input[type="email"]').val("");
        $('input[type="password"]').val("");
        $("#addJenis").val($("#addJenis option:first").val())
    }

    function k(a, d) {
        d = void 0 === d ? 0 : d;
        $.ajax({
            type: "POST",
            url: b + "user/detailUser",
            data: {
                baselink: b,
                id: a
            },
            cache: !1,
            success: function(a) {
                console.log(a);
                a = jQuery.parseJSON(a)[0];
                var c = d;
                c = void 0 === c ? 0 : c;
                0 == c ? (1 == a.id_role ? $("#editJenis").prop("disabled", !0) : $("#editJenis").prop("disabled", !1), $("#editName").val(a.nama), $("#editEmail").val(a.email), $("#editPassword").val(a.password), $("#editRetype").val(a.password), $("#editIdUser").val(a.id_user), $("#editJenis option[value=" + a.id_role + "]").prop("selected", !0), $("#editModal").modal("show")) : ($("#detailName").val(a.nama), $("#detailEmail").val(a.email),
                    $("#detailPassword").val(a.password), $("#detailJenis").val(a.role_name), $("#detailTipe").val(a.tipe), $("#detailCreateAt").val(a.created_at), $("#detailUpdateAt").val(a.updated_at), $("#detailModal").modal("show"))
            },
            error: function(a, d, b) {
                console.log(a.status);
                console.log(a.responseText);
                console.log(b)
            }
        })
    }

    function f(a) {
        0 == a ? ($("#addPanel").slideDown(), $("#viewPanel").slideUp()) : ($("#addPanel").slideUp(), $("#viewPanel").slideDown(), $("#userTable").DataTable().destroy(),
            $("#userTable").DataTable({
                scrollX: !0
            }))
    }
    var b = $("#baselink").val();
    e();
    $(".btnGenerate").click(function() {
        var a = $(this).attr("data-target");
        $.ajax({
            type: "POST",
            url: b + "user/generatePassword",
            data: {
                baselink: b
            },
            cache: !1,
            success: function(d) {
                $("#" + a).html("Password : ");
                $("#" + a).after("<label class='control-label'>" + d + "</label>")
            },
            error: function(a, b, c) {
                console.log(a.status);
                console.log(a.responseText);
                console.log(c)
            }
        })
    });
    $("#addForm").submit(function(a) {
        a.preventDefault();
        $("#addPassword").val() != $("#addRetype").val() ?
            alert("Password tidak sama") : $.ajax({
                type: "POST",
                url: b + "user/addUser",
                data: new FormData(this),
                processData: !1,
                contentType: !1,
                success: function(a) {
                    "success" == jQuery.parseJSON(a).status ? (alert("User berhasil ditambahkan"), f(1), h(), $("#userTable").DataTable().destroy(), e()) : alert("Gagal menambahkan user")
                },
                error: function(a, b, c) {
                    console.log(a.status);
                    console.log(a.responseText);
                    console.log(c)
                }
            })
    });
    $("#listView").on("click", "#editList", function() {
        var a = $(this).attr("data-id");
        k(a)
    });
    $("#listView").on("click",
        "#detailList",
        function() {
            var a = $(this).attr("data-id");
            k(a, 1)
        });
    $("#editForm").submit(function(a) {
        a.preventDefault();
        $("#editPassword").val() != $("#editRetype").val() ? alert("Password tidak sama") : $.ajax({
            type: "POST",
            url: b + "user/editUser",
            data: new FormData(this),
            processData: !1,
            contentType: !1,
            success: function(a) {
                console.log(a);
                "success" == jQuery.parseJSON(a).status ? (alert("Data User berhasil dirubah"), h(), $("#editModal").modal("hide"), $("#userTable").DataTable().destroy(), e()) : alert("Gagal merubah data user")
            },
            error: function(a, b, c) {
                console.log(a.status);
                console.log(a.responseText);
                console.log(c)
            }
        })
    });
    $("#listView").on("click", "#deleteList", function() {
        var a = $(this).attr("data-id");
        $("#modal_delete").modal("show");
        $("#btn_modal").attr("data-id", a)
    });
    $("#btn_modal").click(function() {
        var a = $(this).attr("data-id");
        $.ajax({
            type: "POST",
            url: b + "user/deleteUser",
            data: {
                baselink: b,
                id: a
            },
            cache: !1,
            success: function(a) {
                "success" == jQuery.parseJSON(a).status ? (alert("Datat User Berhasil dihapus"), $("#modal_delete").modal("hide"),
                    $("#userTable").DataTable().destroy(), e()) : alert("Gagal menghapus data user")
            },
            error: function(a, b, c) {
                console.log(a.status);
                console.log(a.responseText);
                console.log(c)
            }
        })
    });
    $("#btn_close").click(function() {
        f(1)
    });
    $("#btn_add").click(function() {
        f(0)
    })
});