$(document).ready(function(){
	var baselink = $("#baselink").val();
    list();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'hrd/list',
			cache: false,
			success: function(response){
				
                var data = jQuery.parseJSON(response);
                $("#listView").html("");
                $.each(data,function(i,item){
                    parseList(item);
                });
                $("#tableHrd").DataTable({autoWidth:false,scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){
        var text =  '<tr>'+
                        '<td>'+data.tanggal+'</td>'+
                        '<td>'+data.periode+'</td>'+
                        '<td>'+data.note+'</td>'+
                         '<td>'+data.status+'</td>';       
        $("#itemAction").find("#itemDownload").attr("data-url",data.upload_file);
        text +="<td style='text-align:center'>"+$("#itemAction").html()+"</td></tr>";
        $("#listView").append(text);
    }
    $("#listView").on('click','#itemDownload',function(){
        var url = $(this).attr('data-url');
        window.open(url, 'name');       
    }); 		
});