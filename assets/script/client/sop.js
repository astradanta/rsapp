$(document).ready(function(){
	var baselink = $("#baselink").val();
		id_client = $("#id_client").val();
		id_pekerjaan = $("#id_pekerjaan").val();
		count = 0;
		itemCount = 0;
		getOnProgress();
	function getOnProgress(){
		$.ajax({
			type: "POST",
			url: baselink+'sop/display',
			data:{"id_client":id_client,"id_pekerjaan":id_pekerjaan},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				parseDataOnProgress(data);

			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function parseDataOnProgress(data){
		var dataCount = 0;
		if(data.count == 0){
			$("#contentOnProgress").html($("#noData").html());
		} else {
			$("#idKerja").val(data.id_kerja);
			$("#containerSumber").html("");
			$("#containerPekerjaan").html("");
			$("#contentOnProgress").html('');
			$("#lblStatus").html("Status : "+data.status);
			filter(data.status);
			count = data.itemData.length;
			$.each(data.itemData,function(i,item){
				if(item.jenis_pekerjaan==0){
					if (item.upload_file == null){
						$("#sumberNoItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#sumberNoItem").find("#uploadButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
						$("#sumberNoItem").find("#uploadButton").attr("data-kerja",data.id_kerja);
						$("#containerSumber").append($("#sumberNoItem").html());
					} else {
						$("#sumberWithItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#sumberWithItem").find("#editButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
						$("#sumberWithItem").find("#editButton").attr("data-kerja",data.id_kerja);
						$("#sumberWithItem").find("#editButton").attr("data-url",item.upload_file);
						$("#sumberWithItem").find("#editButton").attr("data-id",item.id_detail_kerja);
						$("#containerSumber").append($("#sumberWithItem").html());
					}
				} else {
					if(item.is_bool == 0){
						if (item.upload_file == null){
							$("#pekerjaanNoItem").find("#leftName").html(item.nama_detail_pekerjaan);
							$("#containerSumber").append($("#pekerjaanNoItem").html());
						} else {
							$("#pekerjaanWithItem").find("#leftName").html(item.nama_detail_pekerjaan);
							$("#pekerjaanWithItem").find("#downloadButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
							$("#pekerjaanWithItem").find("#downloadButton").attr("data-kerja",data.id_kerja);
							$("#pekerjaanWithItem").find("#downloadButton").attr("data-url",item.upload_file);
							$("#containerSumber").append($("#pekerjaanWithItem").html());
						}
					} else {
						if(item.status == 0){
							$("#pekerjaanBoolNo").find("#leftName").html(item.nama_detail_pekerjaan);													
							$("#containerPekerjaan").append($("#pekerjaanBoolNo").html());
						} else {
							$("#pekerjaanBoolYes").find("#leftName").html(item.nama_detail_pekerjaan);												
							$("#containerPekerjaan").append($("#pekerjaanBoolYes").html());							
						}
					}
				}			
			});
			itemCount = dataCount;
			$("#withData").find($("#lblStatus").html("Status : "+data.status));
			$("#withData").find($("#lblDeadline").html("Deadline : "+data.deadline));
			$("#withData").find($("#lblStartdate").html("Start Date : "+data.start_date));

			$("#contentOnProgress").html($("#withData").html());
		}
	}
	function filter(status){
		if(status == "selesai"){
			$(".btnPanel").attr("style","display:none");
		} else {
			$(".btnPanel").attr("style","");
		}
	}
	$("#containerSumber").on('click','#uploadButton',function(){
		var id_kerja = $(this).attr('data-kerja');
		var id_pekerjaan = $(this).attr('data-pekerjaan');
		$("#input_id_kerja").val(id_kerja);
		$("#input_id_pekerjaan").val(id_pekerjaan);
		$("#manipulateModal").modal("show");		
	});
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data berhasil disimpan");
						$("#manipulateModal").modal("hide");
						getOnProgress();
					}else{
						alert("Gagal menyimpan data");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});
		
	});
	$("#containerSumber").on('click','#editButton',function(){
		var id = $(this).attr('data-id');
		var id_kerja = $(this).attr('data-kerja');
		var id_pekerjaan = $(this).attr('data-pekerjaan');
		var url = $(this).attr('data-url');
		$("#input_id_kerja").val(id_kerja);
		$("#input_id_pekerjaan").val(id_pekerjaan);
		$("#idDetail").val(id);
		$("#manipulateForm").prop('action',baselink+"manageKerja/edit");
		$("#inputUrl").val(url);
		$("#manipulateModal").modal("show");		
	});	
	$("#containerPekerjaan").on('click','#changeButton',function(){
		var id = $(this).attr('data-id');
			status = $(this).attr('data-status');
			if(status == 0 || status == null) {
				$("#secondRadio").prop("checked",true);
			} else {
				$("#firstRadio").prop("checked",true);
			}
			$("#id_detail_kerja").val(id);
		$("#changeModal").modal("show");		
	});		
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	$("#inputUrl").val("");
	  	$("#manipulateForm").prop('action',baselink+"manageKerja/add");
	});	
	$("#containerSumber").on('click','#downloadButton',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});	
	$("#changeStatus").click(function(){
		$("#globalChangeModal").modal("show");
	});
	$("#changeForm").submit(function(e){
			e.preventDefault();
			if(($("#firstRadio").prop('checked')==false) && ($("#secondRadio").prop('checked')==false)){
				alert("pilih salah satu status");
			}  else {
					$.ajax({
					type: "POST",
					url: $(this).prop("action"),
					data: new FormData( this ),
			      	processData: false,
			      	contentType: false,
					success: function(response){
						console.log(response);
						var data = jQuery.parseJSON(response)
						if(data.status == "success"){
							alert("Status Berhasil Diganti");
							getOnProgress();
							$("#changeModal").modal('hide');
						} else {
							alert("Status Ganti Diganti")
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
				        console.log(xhr.status);
				        console.log(xhr.responseText);
				        console.log(thrownError);
				    }
				});			
			}
			
		});
	$("#GlobalChangeForm").submit(function(e){
			e.preventDefault();
			if(($("#thirdRadio").prop('checked')==false) && ($("#fourthRadio").prop('checked')==false)){
				alert("pilih salah satu status");
			}  else {
					$.ajax({
					type: "POST",
					url: $(this).prop("action"),
					data: new FormData( this ),
			      	processData: false,
			      	contentType: false,
					success: function(response){
						console.log(response);
						var data = jQuery.parseJSON(response)
						if(data.status == "success"){
							alert("Status Berhasil Diganti");
							getOnProgress();
							$("#globalChangeModal").modal('hide');
						} else {
							alert("Status Ganti Diganti")
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
				        console.log(xhr.status);
				        console.log(xhr.responseText);
				        console.log(thrownError);
				    }
				});			
			}
			
		});	
	$("#backAction").click(function(e){
		e.preventDefault();
					    var url = baselink+"service/"+ $("#id_client").val();
						window.location.href = url;
	});	
});