$(document).ready(function(){
	var baselink = $("#baselink").val();
	CKEDITOR.replace('catatan');

  $("#periode").datepicker({
    autoclose: true,
    minViewMode: 1,
    language: "id",
    locale: "id",
    format: 'MM yyyy'
	}).on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('.to').datepicker('setStartDate', startDate);
    }); 
    $("#deadline").datepicker({
        autoclose:true
    });
    list();
    listSlip();
    function list(){
        $.ajax({
            type: "POST",
            url: baselink+'penggajian/list',
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                $("#listView").html("");
                $.each(data,function(i,item){
                    parseList(item);
                });
                $("#tableAbsensi").DataTable({autoWidth:false,scrollX:true});
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });         
    }
    function parseList(data){
        var text =  '<tr>'+
                        '<td>'+data.tanggal+'</td>'+
                        '<td>'+data.periode+'</td>'+
                        '<td>'+data.note+'</td>'+
                        '<td>'+data.status+'</td>';
        if(data.pengerjaan_status == 1){
            $("#lockedAction").find("#itemDownload").attr("data-url",data.upload_file);
            text += "<td style='text-align:center'>"+$("#lockedAction").html()+"</td></tr>";
        } else {
            $("#openAction").find("#itemEdit").attr("data-id",data.id_kerja);
            $("#openAction").find("#itemDelete").attr("data-id",data.id_kerja);
            $("#lockedAction").find("#itemDownload").attr("data-url",data.upload_file);
            text += "<td style='text-align:center'>"+$("#openAction").html()+"</td></tr>";
        }
        $("#listView").append(text);
    }
    $("#listView").on('click','#itemDownload',function(){
        var url = $(this).attr('data-url');
        window.open(url, 'name');       
    });
    $("#listView").on('click','#itemEdit',function(){
        var id = $(this).attr('data-id');
        detail(id);
    });
    $("#listView").on('click','#itemDelete',function(e){
        e.preventDefault()
        var id = $(this).attr('data-id');
        $("#modal_delete").modal("show");
        $("#btn_modal").attr('data-id',id);
    }); 
    function detail(id){
        $.ajax({
            type: "POST",
            url: baselink+'penggajian/detail',
            data:{"id":id},
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                parseDetail(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
    function parseDetail(data){
        console.log(data);
        $("#periode").val(data[0].periode);
        $("#deadline").val(data[0].deadline);
        $("#url").val(data[0].upload_file);
        $("#idDetail").val(data[0].id_kerja);
        CKEDITOR.instances.catatan.setData(data[0].note);
        $("#modalTitle").html("Form Ubah Data Absensi");
        $("#manipulateForm").prop("action",baselink+"penggajian/edit");
        $("#manipulateModal").modal("show");
    }
    $("#manipulateModal").on("hidden.bs.modal", function () {
        resetModal();
    });
    function resetModal(){
        $("input[type=text]").val("");
        CKEDITOR.instances.catatan.setData("");
        $("#modalTitle").html("Form Tambah Data Absensi");
        $("#manipulateForm").prop("action",baselink+"penggajian/add");

    }
    $("#manipulateForm").submit(function(e){
        e.preventDefault();
        CKEDITOR.instances.catatan.updateElement();
            $.ajax({
                type: "POST",
                url: $(this).prop("action"),
                data: new FormData( this ),
                processData: false,
                contentType: false,
                success: function(response){
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if (data.status == "success"){
                        alert("Data absensi berhasil disimpan");
                        $("#manipulateModal").modal("hide");
                        $("#tableAbsensi").DataTable().destroy();
                        list();
                    }else{
                        alert("Gagal menyimpan data absensi");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });         
        
    });  
    $("#btn_modal").click(function(){
        var id = $(this).attr("data-id")
        $.ajax({
            type: "POST",
            url: baselink+'penggajian/delete',
            data:{"baselink":baselink,"id":id},
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                if (data.status == "success"){
                    alert("Data Berhasil dihapus");
                    $("#modal_delete").modal("hide");
                    $("#tableAbsensi").DataTable().destroy();
                    list()
                } else {
                    alert("Gagal menghapus data")
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }); 
    function listSlip(){
        $.ajax({
            type: "POST",
            url: baselink+'penggajian/listSlip',
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                $("#slipView").html("");
                $.each(data,function(i,item){
                    parseListSlip(item);
                });
                $("#tableSlip").DataTable({autoWidth:false,scrollX:true});
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });     
    } 
    function parseListSlip(data){
        console.log(data);
        var status = "Diverifikasi";
        if(data.pengerjaan_status == 0){
            status = "Tidak Diverifikasi";
        }
        var text =  '<tr>'+
                        '<td>'+data.tanggal+'</td>'+
                        '<td>'+data.periode+'</td>'+
                        '<td>'+data.note+'</td>'+
                        '<td>'+data.status+'</td>';
        $("#slipAction").find("#itemEdit").attr("data-id",data.id_kerja);
        $("#slipAction").find("#itemDelete").attr("data-id",data.id_kerja);
        $("#slipAction").find("#itemFinish").attr("data-id",data.id_kerja);
        $("#slipAction").find("#itemDownload").attr("data-url",data.upload_file);
         text += '<td style="text-align:center">'+$("#slipAction").html()+'</td></tr>';

        $("#slipView").append(text);
    }
    $("#tabBar1").click(function(){
        $("#tableSlip").DataTable().destroy();
        $("#tableAbsensi").DataTable().destroy();
        $("#tableAbsensi").DataTable({autoWidth:false,scrollX:true});
        $(".dataTables_scrollHeadInner").attr("style",'box-sizing: content-box;width: 100%;padding-right: 0px;');
        $(".dataTable").attr("style","margin-left: 0px;");
    });
    $("#tabBar2").click(function(){
        $("#tableAbsensi").DataTable().destroy();
        $("#tableSlip").DataTable().destroy();
        $("#tableSlip").DataTable({autoWidth:false,scrollX:true});;
        $(".dataTables_scrollHeadInner").attr("style",'box-sizing: content-box;width: 100%;padding-right: 0px;');
        $(".dataTable").attr("style","margin-left: 0px;");
    });    
});