$(document).ready(function(){
	var baselink = $("#baselink").val();
	list();
	CKEDITOR.replace('syarat');
	$('#deadline').datepicker();
	displayList();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'recruit/list',
			cache: false,
			success: function(response){
				console.log(response);
				$("#listView").html('');
				var data = jQuery.parseJSON(response);
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableRequest").DataTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){

		var action = '<button type="button" id="#editItem" data-id="'+data.id_request+'" class="btn btn-warning editItem" style="margin:5px;"><i class="fa fa-pencil"></i></button>'+
                     '<button type="button" data-id="'+data.id_request+'" class="btn btn-danger deleteItem" style="margin:5px;"><i class="fa fa-trash"></i></button>'+
                     '<button id="#viewItem" type="button" data-id="'+data.id_request+'" class="btn btn-info" style="margin:5px;"><i class="fa fa-eye"></i></button>';
        if(data.status > 0) action ='<button id="#viewItem" type="button" data-id="'+data.id_request+'" class="btn btn-info viewItem" style="margin:5px;"><i class="fa fa-eye"></i></button>';

		var text = "<tr>"+						
						"<td>"+data.posisi+"</td>"+
						"<td>"+data.gaji+"</td>"+
						"<td>"+data.jumlah+"</td>"+
						"<td>"+data.syarat+"</td>"+
						"<td>"+data.deadline+"</td>"+
						"<td>"+data.statusLabel+"</td>"+
						"<td></td>"+
						"<td style='text-align:center;'>"+action+"</td>"+
					"</tr>";
		$("#listView").append(text);
	}
	function panelLayout(i){
		if(i == 1 ){
			$("#addContainer").slideDown();
			$("#viewContainer").slideUp();
		} else {
			$("#viewContainer").slideDown();
			$("#addContainer").slideUp();
			$("input[type=text]").val('');
			CKEDITOR.instances.syarat.setData("");
		}
	}
	$("#addButton").click(function(){
		$("#manipulationForm").prop("action",baselink+"recruit/add");
		$("#titleForm").html('Tambah Request');
		panelLayout(1);
	});
	$("#btn_cancel").click(function(){
		panelLayout(0);
	});
	$("#manipulationForm").submit(function(e){
			e.preventDefault();
			var content = CKEDITOR.instances.syarat.getData();
			CKEDITOR.instances.syarat.updateElement();
			if(content == ""){
				alert("syarat/ketentuan tidak boleh kosong");
			}  else {
					$.ajax({
					type: "POST",
					url: $(this).prop("action"),
					data: new FormData( this ),
			      	processData: false,
			      	contentType: false,
					success: function(response){
						var data = jQuery.parseJSON(response)
						if(data.status == "success"){							
							alert("Permintaan Recruitment anda sudah tersimpan");
							panelLayout(0);
							$("tableRequest").DataTable().destroy();
							list();
						} else {
							alert("Gagal menyimpan permintaan recruitment anda");
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
				        console.log(xhr.status);
				        console.log(xhr.responseText);
				        console.log(thrownError);
				    }
				});			
			}
			
		});	
	$("#listView").on('click','.deleteItem',function(){
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#listView").on('click','.editItem',function(){
		$("#manipulationForm").prop("action",baselink+"recruit/edit");
		$("#titleForm").html('Edit Request');		
		var id = $(this).attr('data-id');
		panelLayout(1);
		detail(id);

	});	
	$("#listView").on('click','.viewItem',function(){
		var id = $(this).attr('data-id');
		detail(id,1);
		$("#detailModal").modal("show");
		

	});	
	function detail(id,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'recruit/detail',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				parseDetail(data,type);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseDetail(data,type = 0){
		console.log(data);
		if(type == 0){
			$("#idDetail").val(data[0].id_request);
			$("#posisi").val(data[0].posisi);
			$("#jumlah").val(data[0].jumlah);
			$("#gaji").val(data[0].gaji);
			$("#deadline").val(data[0].deadline);
			CKEDITOR.instances.syarat.setData(data[0].syarat);
		} else {
			$("#detailPosisi").val(data[0].posisi);
			$("#detailGaji").val(data[0].gaji);
			$("#detailJumlah").val(data[0].jumlah);
			$("#detailSyarat").html(data[0].syarat);
			$("#detailStatus").val(data[0].statusLabel);
			$("#detailDeadline").val(data[0].deadline);
			$("#detailCreateAt").val(data[0].created_at);
			$("#detailUpdateAt").val(data[0].updated_at);
		}
	}
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'recruit/delete',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Data Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#tableRequest").DataTable().destroy();
					list()
				} else {
					alert("Gagal menghapus data")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	function displayList(){
		$.ajax({
			type: "POST",
			url: baselink+'recruit/workingView',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);
				$("#workingView").html('');
				$.each(data,function(i,item){
					parseListWorking(item);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function parseListWorking(data){
		var totalKerja = parseInt(data.total_kerja);
		var totalDikerjakan = parseInt(data.total_dikerjakan);
		var persentase = Math.round((totalDikerjakan/totalKerja)*100);
		$("#listItemTemplate").find("#itemLeftIcon").html('<i style="font-size: 40px;">'+persentase+'<sup>%</sup></i>')
		$("#listItemTemplate").find("#itemContainer").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemContainer").attr("data-pekerjaan",data.id_pekerjaan);
		$("#listItemTemplate").find("#itemLeftIcon").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemLeftIcon").attr("data-pekerjaan",data.id_pekerjaan);		
		$("#listItemTemplate").find("#itemlistDeadline").html(data.deadline);
		$("#listItemTemplate").find("#itemlistDeadline").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemlistDeadline").attr("data-pekerjaan",data.id_pekerjaan);
		$("#listItemTemplate").find("#namaPekerjaan").html(data.posisi);
		$("#workingView").append($("#listItemTemplate").html());
	}
	$('#workingView').on('click','#itemContainer',function(e) { 
			var url = baselink+"pekerjaan/"+$(this).attr('data-id')+"/"+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});	
	$('#workingView').on('click','#itemlistDeadline',function(e) { 
			var url = baselink+"pekerjaan/"+$(this).attr('data-id')+"/"+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});	
	$('#workingView').on('click','#itemLeftIcon',function(e) { 
			var url = baselink+"pekerjaan/"+$(this).attr('data-id')+"/"+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});						
});