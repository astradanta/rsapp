$(document).ready(function(){
	var baselink = $("#baselink").val();
	$('#signout').click(function(){
		$.ajax({
			type: "POST",
			url: baselink+'logout',
			cache: false,
			success: function(response){
				data = jQuery.parseJSON(response);
				if (data.status == 1){
					window.location.replace(baselink+'login')
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
	           console.log(xhr.status);
	           console.log(xhr.responseText);
	           console.log(thrownError);
	       }
		});
	});	
});