$(document).ready(function(){
	var baselink = $("#baselink").val();
	list();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);

				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableInvoice").DataTable({scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}	
	function parseList(data){
		var text = 	'<tr>'+
						'<td>'+data.tanggal_invoice+'</td>'+
						'<td>'+data.no_invoice+'</td>'+
						'<td>'+data.due_date+'</td>'+
						'<td>'+data.total+'</td>'+
						'<td>'+data.note+'</td>'+
						'<td>'+data.status+'</td>'+
						'<td style="text-align:center;">'+							
							'<button data-id="'+data.id_invoice+'" class="btn btn-info " id="viewItem" data-url="'+data.file_invoice+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-download"></i>&nbsp;Download</button>'+							
						'</td>'+
					'</tr>';
		$("#listView").append(text);
	}
	$("#listView").on('click','#viewItem',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});			
});