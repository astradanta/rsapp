$( document ).ready(function() {
	var baselink = $('#baselink').val();
	countInvoice();
	setInterval(function(){ 
		countInvoice();
	
	}, 5000);
	list();
	var idPekerjaanKlien = $("#idPekerjaan").val();
	function countInvoice(){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/new',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){				
				var data = jQuery.parseJSON(response);
				if (data.length > 0){
					$("#invoiceCount").html(data.length);
				} else {
					$("#invoiceCount").html('');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		       	console.log(xhr.status);
		       	console.log(xhr.responseText);
		       	console.log(thrownError);
			}
		});			
	}
	$("#invoicePanel").on('click',function(){
		window.location.href = baselink+'invoice';
	});
	$("#confirmPanel").on('click',function(){
		window.location.href = baselink+'confirm';
	});	
	$("#saranPanel").on('click',function(){
		window.location.href = baselink+'saran';
	});	
	function list()		{
		$.ajax({
			type: "POST",
			url: baselink+'/listPekerjaan',
			cache: false,
			success: function(response){				
				console.log(response);
				var data = jQuery.parseJSON(response);
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		       	console.log(xhr.status);
		       	console.log(xhr.responseText);
		       	console.log(thrownError);
			}
		});		
	}
	function parseList(data){
		var totalKerja = parseInt(data.total_kerja);
		var totalDikerjakan = parseInt(data.total_dikerjakan);
		var persentase = Math.round((totalDikerjakan/totalKerja)*100);
		$("#listItemTemplate").find($("#itemContainer").attr("data-id",data.id_kerja));
		$("#listItemTemplate").find($("#itemContainer").attr("data-pekerjaan",data.id_pekerjaan));
		$("#listItemTemplate").find($("#itemLeftIcon").attr("data-id",data.id_kerja));
		$("#listItemTemplate").find($("#itemLeftIcon").attr("data-pekerjaan",data.id_pekerjaan));		
		$("#listItemTemplate").find($("#itemlistDeadline").html(data.deadline));
		$("#listItemTemplate").find($("#itemlistDeadline").attr("data-id",data.id_kerja));
		$("#listItemTemplate").find($("#itemlistDeadline").attr("data-pekerjaan",data.id_pekerjaan));
		$("#listItemTemplate").find($("#jenisPekerjaan").html(data.nama_pekerjaan));
		$("#listItemTemplate").find($("#namaPekerjaan").html(data.nama_kerja));
		$("#listView").append($("#listItemTemplate").html());
	}
	$('#listView').on('click','#itemContainer',function(e) { 
			var id_pekerjaan = $(this).attr("data-pekerjaan");
			var id_kerja = $(this).attr("data-id");
			var url = baselink;
			if(id_pekerjaan == 1){
				url +="assessment/"+id_kerja;
			} else {
				url +="kom_product/"+id_kerja;
			}
			window.location.href = url;				
	});	
	$('#listView').on('click','#itemlistDeadline',function(e) { 
			var id_pekerjaan = $(this).attr("data-pekerjaan");
			var id_kerja = $(this).attr("data-id");
			var url = baselink;
			if(id_pekerjaan == 1){
				url +="assessment/"+id_kerja;
			} else {
				url +="kom_product/"+id_kerja;
			}
			window.location.href = url;	
	});	
	$('#listView').on('click','#itemLeftIcon',function(e) { 
			var id_pekerjaan = $(this).attr("data-pekerjaan");
			var id_kerja = $(this).attr("data-id");
			var url = baselink;
			if(id_pekerjaan == 1){
				url +="assessment/"+id_kerja;
			} else {
				url +="kom_product/"+id_kerja;
			}
			window.location.href = url;		
	});	
});