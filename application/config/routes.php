<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'B_Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// login
$route['login'] = 'Back_office/B_Login';
$route['login/signin'] = 'Back_office/B_Login/signIn';
$route['login/forget'] = 'Back_office/B_Login/forget';
$route['login/acx'] = 'Back_office/B_Login/acx';


$route['logout'] = 'B_Home/logout';
$route['listClient'] = 'B_Home/listClient';
$route['listPekerjaan'] = 'B_Home/listPekerjaan';
$route['filterClient'] = 'B_Home/filterClient';
$route['addClient'] = 'B_Home/addClient';


$route['deleteClient'] = 'B_Home/deleteClient';
$route['countElement'] = 'B_Home/countElement';
$route['home'] = 'B_Home';

// Back office log //
$route['log'] = 'Back_office/B_Log';
$route['log/loadData'] = 'Back_office/B_Log/loadData';
$route['log/currentLog'] = 'Back_office/B_Log/currentLog';

// Back  office user//
$route['user'] = 'Back_office/B_User';
$route['user/listUser'] = 'Back_office/B_User/listUser';
$route['user/addUser'] = 'Back_office/B_User/addUser';
$route['user/detailUser'] = 'Back_office/B_User/detailUser';
$route['user/editUser'] = 'Back_office/B_User/editUser';
$route['user/deleteUser'] = 'Back_office/B_User/deleteUser';
$route['user/generatePassword'] = 'Back_office/B_User/generatePassword';
$route['user/acx'] = 'Back_office/B_User/acx';

// Back  office pekerjaan//
$route['pekerjaan/:num'] = 'Back_office/B_Pekerjaan';
$route['pekerjaan/list'] = 'Back_office/B_Pekerjaan/list';
$route['pekerjaan/add'] = 'Back_office/B_Pekerjaan/add';
$route['pekerjaan/detail'] = 'Back_office/B_Pekerjaan/detail';
$route['pekerjaan/edit'] = 'Back_office/B_Pekerjaan/edit';
$route['pekerjaan/delete'] = 'Back_office/B_Pekerjaan/delete';
$route['pekerjaan/change'] = 'Back_office/B_Pekerjaan/change';



// Back  office pekerjaan klien//
$route['pekerjaanKlien/:num'] = 'Back_office/B_PekerjaanKlien';
$route['pekerjaanKlien/list'] = 'Back_office/B_PekerjaanKlien/list';
$route['pekerjaanKlien/add'] = 'Back_office/B_PekerjaanKlien/add';
$route['pekerjaanKlien/detail'] = 'Back_office/B_PekerjaanKlien/detail';
$route['pekerjaanKlien/edit'] = 'Back_office/B_PekerjaanKlien/edit';
$route['pekerjaanKlien/delete'] = 'Back_office/B_PekerjaanKlien/delete';
$route['pekerjaanKlien/listSelectPekerjaan'] = 'Back_office/B_PekerjaanKlien/listSelectPekerjaan';
$route['pekerjaanKlien/listPekerjaanKlien'] = 'Back_office/B_PekerjaanKlien/listPekerjaanKlien';

//Back office history pekerjaan//
$route['historyPekerjaan'] = 'Back_office/B_HistoryPekerjaan';
$route['historyPekerjaan/list'] = 'Back_office/B_HistoryPekerjaan/list';
$route['historyPekerjaan/search'] = 'Back_office/B_HistoryPekerjaan/search';
$route['historyPekerjaan/detail'] = 'Back_office/B_HistoryPekerjaan/detail';
$route['detailHistory/:num'] = 'Back_office/B_DetailHistory';



//Back office history pekerjaan//
$route['invoice'] = 'Back_office/B_Invoice';
$route['invoice/list'] = 'Back_office/B_Invoice/list';
$route['invoice/archive'] = 'Back_office/B_Invoice/archive';
$route['invoice/getNoInvoice'] = 'Back_office/B_Invoice/getNoInvoice';
$route['invoice/add'] = 'Back_office/B_Invoice/add';
$route['invoice/detail'] = 'Back_office/B_Invoice/detail';
$route['invoice/edit'] = 'Back_office/B_Invoice/edit';
$route['invoice/delete'] = 'Back_office/B_Invoice/delete';
$route['invoice/cancel'] = 'Back_office/B_Invoice/cancel';

//Back office konfirmasi pembayaran//
$route['confirm'] = 'Back_office/B_Confirm';
$route['confirm/list'] = 'Back_office/B_Confirm/list';
$route['confirm/changeStatus'] = 'Back_office/B_Confirm/changeStatus';

//Back office konfirmasi pembayaran//
$route['receipt'] = 'Back_office/B_Receipt';
$route['receipt/list'] = 'Back_office/B_Receipt/list';
$route['receipt/listInvoice'] = 'Back_office/B_Receipt/listInvoice';
$route['receipt/add'] = 'Back_office/B_Receipt/add';
$route['receipt/detail'] = 'Back_office/B_Receipt/detail';
$route['receipt/edit'] = 'Back_office/B_Receipt/edit';
$route['receipt/delete'] = 'Back_office/B_Receipt/delete';

$route['saran'] = 'Back_office/B_Saran';
$route['saran/list'] = 'Back_office/B_Saran/list';
$route['saran/delete'] = 'Back_office/B_Saran/delete';


$route['manageKerja/:num/:num/:num'] = 'Back_office/B_ManageKerja';
$route['manageKerja/add'] = 'Back_office/B_ManageKerja/add';
$route['manageKerja/edit'] = 'Back_office/B_ManageKerja/edit';
$route['manageKerja/changeDetail'] = 'Back_office/B_ManageKerja/changeDetail';
$route['manageKerja/changeStatus'] = 'Back_office/B_ManageKerja/changeStatus';

$route['manageClient'] = 'Back_office/B_Client';
$route['manageClient/countElement'] = 'Back_office/B_Client/countElement';
$route['manageClient/pekerjaanKlien'] = 'Back_office/B_Client/pekerjaanKlien';
$route['manageClient/savePekerjaan'] = 'Back_office/B_Client/savePekerjaan';
$route['manageClient/addClient'] = 'Back_office/B_Client/addClient';
$route['manageClient/listClient'] = 'Back_office/B_Client/listClient';
$route['manageClient/detailClient'] = 'Back_office/B_Client/detailClient';
$route['manageClient/editClient'] = 'Back_office/B_Client/editClient';

$route['assessment/:num'] = 'Back_office/B_Assessment';
$route['assessment/add'] = 'Back_office/B_Assessment/add';
$route['assessment/edit'] = 'Back_office/B_Assessment/edit';
$route['assessment/list'] = 'Back_office/B_Assessment/list';
$route['assessment/delete'] = 'Back_office/B_Assessment/delete';
$route['assessment/detail'] = 'Back_office/B_Assessment/detail';

$route['kom/:num'] = 'Back_office/B_Kom';
$route['kom/add'] = 'Back_office/B_Kom/add';
$route['kom/edit'] = 'Back_office/B_Kom/edit';
$route['kom/list'] = 'Back_office/B_Kom/list';
$route['kom/delete'] = 'Back_office/B_Kom/delete';
$route['kom/detail'] = 'Back_office/B_Kom/detail';

$route['developt/:num'] = 'Back_office/B_Developt';
$route['developt/add'] = 'Back_office/B_Developt/add';
$route['developt/edit'] = 'Back_office/B_Developt/edit';
$route['developt/list'] = 'Back_office/B_Developt/list';
$route['developt/delete'] = 'Back_office/B_Developt/delete';
$route['developt/detail'] = 'Back_office/B_Developt/detail';
$route['developt/change'] = 'Back_office/B_Developt/change';

$route['installation/:num'] = 'Back_office/B_Installation';
$route['installation/add'] = 'Back_office/B_Installation/add';
$route['installation/edit'] = 'Back_office/B_Installation/edit';
$route['installation/list'] = 'Back_office/B_Installation/list';
$route['installation/delete'] = 'Back_office/B_Installation/delete';
$route['installation/detail'] = 'Back_office/B_Installation/detail';
$route['installation/change'] = 'Back_office/B_Installation/change';

$route['tutorial/:num'] = 'Back_office/B_Tutorial';
$route['tutorial/add'] = 'Back_office/B_Tutorial/add';
$route['tutorial/edit'] = 'Back_office/B_Tutorial/edit';
$route['tutorial/list'] = 'Back_office/B_Tutorial/list';
$route['tutorial/delete'] = 'Back_office/B_Tutorial/delete';
$route['tutorial/detail'] = 'Back_office/B_Tutorial/detail';
$route['tutorial/change'] = 'Back_office/B_Tutorial/change';

$route['bug/:num'] = 'Back_office/B_Bug';
$route['bug/add'] = 'Back_office/B_Bug/add';
$route['bug/edit'] = 'Back_office/B_Bug/edit';
$route['bug/list'] = 'Back_office/B_Bug/list';
$route['bug/delete'] = 'Back_office/B_Bug/delete';
$route['bug/detail'] = 'Back_office/B_Bug/detail';

$route['systemRunning/:num'] = 'Back_office/B_System';
$route['systemRunning/add'] = 'Back_office/B_System/add';
$route['systemRunning/edit'] = 'Back_office/B_System/edit';
$route['systemRunning/list'] = 'Back_office/B_System/list';
$route['systemRunning/delete'] = 'Back_office/B_System/delete';
$route['systemRunning/detail'] = 'Back_office/B_System/detail';
$route['systemRunning/change'] = 'Back_office/B_System/change';

$route['maintenance/:num'] = 'Back_office/B_Maintenance';
$route['maintenance/add'] = 'Back_office/B_Maintenance/add';
$route['maintenance/edit'] = 'Back_office/B_Maintenance/edit';
$route['maintenance/list'] = 'Back_office/B_Maintenance/list';
$route['maintenance/delete'] = 'Back_office/B_Maintenance/delete';
$route['maintenance/detail'] = 'Back_office/B_Maintenance/detail';

$route['kom_product/:num'] = 'Back_office/B_Kom_product';
$route['kom_product/add'] = 'Back_office/B_Kom_product/add';
$route['kom_product/edit'] = 'Back_office/B_Kom_product/edit';
$route['kom_product/list'] = 'Back_office/B_Kom_product/list';
$route['kom_product/delete'] = 'Back_office/B_Kom_product/delete';
$route['kom_product/detail'] = 'Back_office/B_Kom_product/detail';

$route['developt_product/:num'] = 'Back_office/B_Developt_product';
$route['developt_product/add'] = 'Back_office/B_Developt_product/add';
$route['developt_product/edit'] = 'Back_office/B_Developt_product/edit';
$route['developt_product/list'] = 'Back_office/B_Developt_product/list';
$route['developt_product/delete'] = 'Back_office/B_Developt_product/delete';
$route['developt_product/detail'] = 'Back_office/B_Developt_product/detail';
$route['developt_product/change'] = 'Back_office/B_Developt_product/change';

$route['installation_product/:num'] = 'Back_office/B_Installation_product';
$route['installation_product/add'] = 'Back_office/B_Installation_product/add';
$route['installation_product/edit'] = 'Back_office/B_Installation_product/edit';
$route['installation_product/list'] = 'Back_office/B_Installation_product/list';
$route['installation_product/delete'] = 'Back_office/B_Installation_product/delete';
$route['installation_product/detail'] = 'Back_office/B_Installation_product/detail';
$route['installation_product/change'] = 'Back_office/B_Installation_product/change';

$route['tutorial_product/:num'] = 'Back_office/B_Tutorial_product';
$route['tutorial_product/add'] = 'Back_office/B_Tutorial_product/add';
$route['tutorial_product/edit'] = 'Back_office/B_Tutorial_product/edit';
$route['tutorial_product/list'] = 'Back_office/B_Tutorial_product/list';
$route['tutorial_product/delete'] = 'Back_office/B_Tutorial_product/delete';
$route['tutorial_product/detail'] = 'Back_office/B_Tutorial_product/detail';
$route['tutorial_product/change'] = 'Back_office/B_Tutorial_product/change';

$route['input_product/:num'] = 'Back_office/B_Input_product';
$route['input_product/add'] = 'Back_office/B_Input_product/add';
$route['input_product/edit'] = 'Back_office/B_Input_product/edit';
$route['input_product/list'] = 'Back_office/B_Input_product/list';
$route['input_product/delete'] = 'Back_office/B_Input_product/delete';
$route['input_product/detail'] = 'Back_office/B_Input_product/detail';
$route['input_product/change'] = 'Back_office/B_Input_product/change';

$route['bug_product/:num'] = 'Back_office/B_Bug_product';
$route['bug_product/add'] = 'Back_office/B_Bug_product/add';
$route['bug_product/edit'] = 'Back_office/B_Bug_product/edit';
$route['bug_product/list'] = 'Back_office/B_Bug_product/list';
$route['bug_product/delete'] = 'Back_office/B_Bug_product/delete';
$route['bug_product/detail'] = 'Back_office/B_Bug_product/detail';

$route['system_product/:num'] = 'Back_office/B_System_product';
$route['system_product/add'] = 'Back_office/B_System_product/add';
$route['system_product/edit'] = 'Back_office/B_System_product/edit';
$route['system_product/list'] = 'Back_office/B_System_product/list';
$route['system_product/delete'] = 'Back_office/B_System_product/delete';
$route['system_product/detail'] = 'Back_office/B_System_product/detail';
$route['system_product/change'] = 'Back_office/B_System_product/change';

$route['maintenance_product/:num'] = 'Back_office/B_Maintenance_product';
$route['maintenance_product/add'] = 'Back_office/B_Maintenance_product/add';
$route['maintenance_product/edit'] = 'Back_office/B_Maintenance_product/edit';
$route['maintenance_product/list'] = 'Back_office/B_Maintenance_product/list';
$route['maintenance_product/delete'] = 'Back_office/B_Maintenance_product/delete';
$route['maintenance_product/detail'] = 'Back_office/B_Maintenance_product/detail';

//CLIENT//


// Client Route
$route['client'] = 'Client/C_Home';
$route['client/login'] = 'Client/C_Login';
$route['client/signin'] = 'Client/C_Login/signIn';
$route['client/forget'] = 'Client/C_Login/forget';
$route['client/logout'] = 'Client/C_Home/logout';
$route['client/listPekerjaan'] = 'Client/C_Home/listPekerjaan';

$route['sendEmail'] = 'Back_office/B_Login/sendPasswordWithEmail';

// Client log //
$route['client/log'] = 'Client/C_Log';
$route['client/log/loadData'] = 'Client/C_Log/loadData';


// client pekerjaan //
// Back  office detail pekerjaan//
$route['client/pekerjaan/:num/:num'] = 'Client/C_Pekerjaan';
$route['client/pekerjaan/getOnProgress'] = 'Client/C_Pekerjaan/getOnProgress';
$route['client/pekerjaan/add'] = 'Client/C_Pekerjaan/add';
$route['client/pekerjaan/edit'] = 'Client/C_Pekerjaan/edit';
$route['client/pekerjaan/getArchive'] = 'Client/C_Pekerjaan/getArchive';

//client invoice//
$route['client/invoice'] = 'Client/C_Invoice';
$route['client/invoice/list'] = 'Client/C_Invoice/list';
$route['client/invoice/new'] = 'Client/C_Invoice/new';

//client konfirmasi//
$route['client/confirm'] = 'Client/C_Confirm';
$route['client/confirm/list'] = 'Client/C_Confirm/list';
$route['client/confirm/add'] = 'Client/C_Confirm/add';
$route['client/confirm/detail'] = 'Client/C_Confirm/detail';
$route['client/confirm/edit'] = 'Client/C_Confirm/edit';
$route['client/confirm/delete'] = 'Client/C_Confirm/delete';
$route['client/confirm/listReceipt'] = 'Client/C_Confirm/listReceipt';

$route['client/saran'] = 'Client/C_Saran';
$route['client/saran/list'] = 'Client/C_Saran/list';
$route['client/saran/add'] = 'Client/C_Saran/add';
$route['client/saran/edit'] = 'Client/C_Saran/edit';
$route['client/saran/delete'] = 'Client/C_Saran/delete';

$route['client/assessment/:num'] = 'Client/C_Assessment';
$route['client/assessment/add'] = 'Client/C_Assessment/add';
$route['client/assessment/edit'] = 'Client/C_Assessment/edit';
$route['client/assessment/list'] = 'Client/C_Assessment/list';
$route['client/assessment/delete'] = 'Client/C_Assessment/delete';
$route['client/assessment/detail'] = 'Client/C_Assessment/detail';

$route['client/kom/:num'] = 'Client/C_Kom';
$route['client/kom/add'] = 'Client/C_Kom/add';
$route['client/kom/edit'] = 'Client/C_Kom/edit';
$route['client/kom/list'] = 'Client/C_Kom/list';
$route['client/kom/delete'] = 'Client/C_Kom/delete';
$route['client/kom/detail'] = 'Client/C_Kom/detail';

$route['client/developt/:num'] = 'Client/C_Developt';
$route['client/developt/add'] = 'Client/C_Developt/add';
$route['client/developt/edit'] = 'Client/C_Developt/edit';
$route['client/developt/list'] = 'Client/C_Developt/list';
$route['client/developt/delete'] = 'Client/C_Developt/delete';
$route['client/developt/detail'] = 'Client/C_Developt/detail';
$route['client/developt/change'] = 'Client/C_Developt/change';

$route['client/installation/:num'] = 'Client/C_Installation';
$route['client/installation/add'] = 'Client/C_Installation/add';
$route['client/installation/edit'] = 'Client/C_Installation/edit';
$route['client/installation/list'] = 'Client/C_Installation/list';
$route['client/installation/delete'] = 'Client/C_Installation/delete';
$route['client/installation/detail'] = 'Client/C_Installation/detail';
$route['client/installation/change'] = 'Client/C_Installation/change';

$route['client/tutorial/:num'] = 'Client/C_Tutorial';
$route['client/tutorial/add'] = 'Client/C_Tutorial/add';
$route['client/tutorial/edit'] = 'Client/C_Tutorial/edit';
$route['client/tutorial/list'] = 'Client/C_Tutorial/list';
$route['client/tutorial/delete'] = 'Client/C_Tutorial/delete';
$route['client/tutorial/detail'] = 'Client/C_Tutorial/detail';
$route['client/tutorial/change'] = 'Client/C_Tutorial/change';

$route['client/bug/:num'] = 'Client/C_Bug';
$route['client/bug/add'] = 'Client/C_Bug/add';
$route['client/bug/edit'] = 'Client/C_Bug/edit';
$route['client/bug/list'] = 'Client/C_Bug/list';
$route['client/bug/delete'] = 'Client/C_Bug/delete';
$route['client/bug/detail'] = 'Client/C_Bug/detail';

$route['client/systemRunning/:num'] = 'Client/C_System';
$route['client/systemRunning/add'] = 'Client/C_System/add';
$route['client/systemRunning/edit'] = 'Client/C_System/edit';
$route['client/systemRunning/list'] = 'Client/C_System/list';
$route['client/systemRunning/delete'] = 'Client/C_System/delete';
$route['client/systemRunning/detail'] = 'Client/C_System/detail';
$route['client/systemRunning/change'] = 'Client/C_System/change';

$route['client/maintenance/:num'] = 'Client/C_Maintenance';
$route['client/maintenance/add'] = 'Client/C_Maintenance/add';
$route['client/maintenance/edit'] = 'Client/C_Maintenance/edit';
$route['client/maintenance/list'] = 'Client/C_Maintenance/list';
$route['client/maintenance/delete'] = 'Client/C_Maintenance/delete';
$route['client/maintenance/detail'] = 'Client/C_Maintenance/detail';

$route['client/kom_product/:num'] = 'Client/C_Kom_product';
$route['client/kom_product/add'] = 'Client/C_Kom_product/add';
$route['client/kom_product/edit'] = 'Client/C_Kom_product/edit';
$route['client/kom_product/list'] = 'Client/C_Kom_product/list';
$route['client/kom_product/delete'] = 'Client/C_Kom_product/delete';
$route['client/kom_product/detail'] = 'Client/C_Kom_product/detail';

$route['client/developt_product/:num'] = 'Client/C_Developt_product';
$route['client/developt_product/add'] = 'Client/C_Developt_product/add';
$route['client/developt_product/edit'] = 'Client/C_Developt_product/edit';
$route['client/developt_product/list'] = 'Client/C_Developt_product/list';
$route['client/developt_product/delete'] = 'Client/C_Developt_product/delete';
$route['client/developt_product/detail'] = 'Client/C_Developt_product/detail';
$route['client/developt_product/change'] = 'Client/C_Developt_product/change';

$route['client/installation_product/:num'] = 'Client/C_Installation_product';
$route['client/installation_product/add'] = 'Client/C_Installation_product/add';
$route['client/installation_product/edit'] = 'Client/C_Installation_product/edit';
$route['client/installation_product/list'] = 'Client/C_Installation_product/list';
$route['client/installation_product/delete'] = 'Client/C_Installation_product/delete';
$route['client/installation_product/detail'] = 'Client/C_Installation_product/detail';
$route['client/installation_product/change'] = 'Client/C_Installation_product/change';

$route['client/tutorial_product/:num'] = 'Client/C_Tutorial_product';
$route['client/tutorial_product/add'] = 'Client/C_Tutorial_product/add';
$route['client/tutorial_product/edit'] = 'Client/C_Tutorial_product/edit';
$route['client/tutorial_product/list'] = 'Client/C_Tutorial_product/list';
$route['client/tutorial_product/delete'] = 'Client/C_Tutorial_product/delete';
$route['client/tutorial_product/detail'] = 'Client/C_Tutorial_product/detail';
$route['client/tutorial_product/change'] = 'Client/C_Tutorial_product/change';

$route['client/input_product/:num'] = 'Client/C_Input_product';
$route['client/input_product/add'] = 'Client/C_Input_product/add';
$route['client/input_product/edit'] = 'Client/C_Input_product/edit';
$route['client/input_product/list'] = 'Client/C_Input_product/list';
$route['client/input_product/delete'] = 'Client/C_Input_product/delete';
$route['client/input_product/detail'] = 'Client/C_Input_product/detail';
$route['client/input_product/change'] = 'Client/C_Input_product/change';

$route['client/bug_product/:num'] = 'Client/C_Bug_product';
$route['client/bug_product/add'] = 'Client/C_Bug_product/add';
$route['client/bug_product/edit'] = 'Client/C_Bug_product/edit';
$route['client/bug_product/list'] = 'Client/C_Bug_product/list';
$route['client/bug_product/delete'] = 'Client/C_Bug_product/delete';
$route['client/bug_product/detail'] = 'Client/C_Bug_product/detail';

$route['client/system_product/:num'] = 'Client/C_System_product';
$route['client/system_product/add'] = 'Client/C_System_product/add';
$route['client/system_product/edit'] = 'Client/C_System_product/edit';
$route['client/system_product/list'] = 'Client/C_System_product/list';
$route['client/system_product/delete'] = 'Client/C_System_product/delete';
$route['client/system_product/detail'] = 'Client/C_System_product/detail';
$route['client/system_product/change'] = 'Client/C_System_product/change';

$route['client/maintenance_product/:num'] = 'Client/C_Maintenance_product';
$route['client/maintenance_product/add'] = 'Client/C_Maintenance_product/add';
$route['client/maintenance_product/edit'] = 'Client/C_Maintenance_product/edit';
$route['client/maintenance_product/list'] = 'Client/C_Maintenance_product/list';
$route['client/maintenance_product/delete'] = 'Client/C_Maintenance_product/delete';
$route['client/maintenance_product/detail'] = 'Client/C_Maintenance_product/detail';
