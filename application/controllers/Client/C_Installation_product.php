<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Installation_product extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_manage_kerja','',TRUE);
		$this->load->model('m_log','',TRUE);
	}
	public function index()
	{
		$id_kerja = $this->uri->segment(3);
		$dataKerja = $this->m_manage_kerja->getDataKerja($id_kerja);
		if(sizeof($dataKerja) < 1) redirect(base_url());
		$id_pekerjaan = $dataKerja[0]->id_pekerjaan;
		$data['id_kerja'] = $id_kerja;
		$data['id_pekerjaan'] = $id_pekerjaan;
		$data['nama_client'] = $dataKerja[0]->nama_client;
		$data['id_client'] = $dataKerja[0]->id_client;
		$data['pic'] = $dataKerja[0]->pic;
		$data['start_date'] = $dataKerja[0]->start_date;
		$data['deadline'] = $dataKerja[0]->deadline;
		$data['status_kerja'] = "selesai";
		$data['nama_kerja'] = $dataKerja[0]->nama_kerja;
		$this->load->view('Client/static/header',$data);
		$this->load->view('Client/static/navbar');
		$this->load->view('Back_office/installation_product');
		$this->load->view('Back_office/static/footer');
	}
	function add(){
		$result['status'] = "failed";
		if(isset($_SESSION['idClient'])&&isset($_POST['id_kerja'])){
			$id_kerja = $_POST['id_kerja'];
			$time = strtotime($_POST['tanggal']);
			$date = date('Y-m-d',$time);
			$note = $_POST['deskripsi'];
			$nama = $_POST['nama'];
			$insert = $this->m_manage_kerja->addInstallationProduct($id_kerja,$note,$date,$nama);
			if($insert){
				$result['status'] = "success";
				$date = date('Y-m-d');				
				$this->m_log->insertLog($_SESSION['idClient'],'Tambah data installation list',$date);
			}
		}
		echo(json_encode($result));
	}

	function list(){
		if(isset($_POST['id_kerja'])&& isset($_SESSION['idClient'])){
			$id_kerja = $_POST['id_kerja'];
			$data = $this->m_manage_kerja->listInstallationProduct($id_kerja);
			foreach ($data as $key) {
				$time = strtotime($key->date);
				$key->date = date('d-m-Y',$time);
				$time = strtotime($key->deadline);
				$key->deadline = date('d-m-Y',$time);
				$key->labelStatus = "On Progress";
				if($key->status == 1) $key->labelStatus = "Selesai";
			}
			echo(json_encode($data));
		}
	}
	function detail(){
		if(isset($_SESSION['idClient'])&& isset($_POST['id_detail_kerja'])){
			$id_detail_kerja = $_POST['id_detail_kerja'];
			$data = $this->m_manage_kerja->detailDevelopt($id_detail_kerja);
			echo json_encode($data);
		}
	}
	function edit(){
		$result['status'] = "failed";
		if(isset($_SESSION['idClient'])&&isset($_POST['id_kerja'])){
			$id_detail_kerja = $_POST['id_detail_kerja'];
			$time = strtotime($_POST['tanggal']);
			$date = date('Y-m-d',$time);
			$note = $_POST['deskripsi'];
			$nama = $_POST['nama'];

			$edit = $this->m_manage_kerja->editInstallationProduct($id_detail_kerja,$note,$date,$nama);
			if($edit){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Edit data installation list',$date);
			}
		}
		echo(json_encode($result));		
	}
	function delete(){
		$result['status'] = "failed";
		if(isset($_SESSION['idClient'])&&isset($_POST['id_detail_kerja'])){
			$id_detail_kerja = $_POST['id_detail_kerja'];
			$delete = $this->m_manage_kerja->deleteKom($id_detail_kerja);
			if($delete){
				$result['status'] = "success";
				$date = date('Y-m-d');				
				$this->m_log->insertLog($_SESSION['idClient'],'Delete  Developtment List',$date);
			}
		}
		echo json_encode($result);
	}
	function change(){
		$result['status'] = "failed";
		if(isset($_SESSION['idClient'])&&isset($_POST['id_detail_kerja'])){
			$id_detail_kerja = $_POST['id_detail_kerja'];
			$status = $_POST['status'];
			$change = $this->m_manage_kerja->changeDevelopt($id_detail_kerja,$status);
			if($change){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$temp = "on progress";
				if($status == 1) $temp = "selesai";				
				$this->m_log->insertLog($_SESSION['idClient'],'Mengganti status developtment list menjadi '.$temp,$date);
			}
		}
		echo json_encode($result);		
	}
}
