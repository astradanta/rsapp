<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Log extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
	}
	public function index()
	{
		$this->load->view('Client/static/header');
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/log');
		$this->load->view('Client/static/footer');
		if(!isset($_SESSION['namaClient'])){
			redirect(base_url().'client/login');
		}

	}

	function loadData()	{
		$id = $_SESSION['idClient'];
		$data = $this->m_log->loadLogUser($id);
		echo json_encode($data);
	}
}
