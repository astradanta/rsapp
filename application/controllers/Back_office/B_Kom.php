<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Kom extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_manage_kerja','',TRUE);
		$this->load->model('m_log','',TRUE);
	}
	public function index()
	{
		$id_kerja = $this->uri->segment(2);
		$dataKerja = $this->m_manage_kerja->getDataKerja($id_kerja);
		if(sizeof($dataKerja) < 1) redirect(base_url());
		$id_pekerjaan = $dataKerja[0]->id_pekerjaan;
		$data['id_kerja'] = $id_kerja;
		$data['id_pekerjaan'] = $id_pekerjaan;
		$data['nama_client'] = $dataKerja[0]->nama_client;
		$data['id_client'] = $dataKerja[0]->id_client;
		$data['pic'] = $dataKerja[0]->pic;
		$data['start_date'] = $dataKerja[0]->start_date;
		$data['deadline'] = $dataKerja[0]->deadline;
		$data['status_kerja'] = $dataKerja[0]->status;
		$data['isPekerjaan'] = 1;
		$data['nama_kerja'] = $dataKerja[0]->nama_kerja;
		$this->load->view('Back_office/static/header',$data);
		$this->load->view('Back_office/static/sidebar');
		//$this->load->view('Back_office/static/projectSidebar');
		$this->load->view('Back_office/kom');
		$this->load->view('Back_office/static/footer');
	}
	function add(){
		$result['status'] = "failed";
		if(isset($_SESSION['id'])&&isset($_POST['id_kerja'])){
			$id_kerja = $_POST['id_kerja'];
			$time = strtotime($_POST['tanggal']);
			$date = date('Y-m-d',$time);
			$note = $_POST['deskripsi'];
			$upload_file = $_POST['file'];
			$id_sub_detail_pekerjaan = $_POST['id_sub_detail_pekerjaan'];
			$insert = $this->m_manage_kerja->addKomProject($id_kerja,$upload_file,$note,$id_sub_detail_pekerjaan,$date);
			if($insert){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$sub = "Dokumentasi Absen";
				if($id_sub_detail_pekerjaan == 2) $sub = "MOM";
				$this->m_log->insertLog($_SESSION['id'],'Tambah '.$sub.' Assessment',$date);
			}
		}
		echo(json_encode($result));
	}

	function list(){
		if(isset($_POST['id_kerja'])&& isset($_SESSION['id'])){
			$id_kerja = $_POST['id_kerja'];
			$data = $this->m_manage_kerja->listKom($id_kerja);
			foreach ($data as $key) {
				$time = strtotime($key->date);
				$date = date('d-m-Y',$time);
			}
			echo(json_encode($data));
		}
	}
	function detail(){
		if(isset($_SESSION['id'])&& isset($_POST['id_detail_kerja'])){
			$id_detail_kerja = $_POST['id_detail_kerja'];
			$data = $this->m_manage_kerja->detailKom($id_detail_kerja);
			echo json_encode($data);
		}
	}
	function edit(){
		$result['status'] = "failed";
		if(isset($_SESSION['id'])&&isset($_POST['id_kerja'])){
			$id_detail_kerja = $_POST['id_detail_kerja'];
			$time = strtotime($_POST['tanggal']);
			$date = date('Y-m-d',$time);
			$note = $_POST['deskripsi'];
			$upload_file = $_POST['file'];
			$edit = $this->m_manage_kerja->editKomProject($id_detail_kerja,$upload_file,$note,$date);
			if($edit){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$sub = "Dokumentasi Absen";
				if($_POST['id_sub_detail_pekerjaan'] == 2) $sub = "MOM";
				$this->m_log->insertLog($_SESSION['id'],'Edit '.$sub.' Assessment',$date);
			}
		}
		echo(json_encode($result));		
	}
	function delete(){
		$result['status'] = "failed";
		if(isset($_SESSION['id'])&&isset($_POST['id_detail_kerja'])){
			$id_detail_kerja = $_POST['id_detail_kerja'];
			$delete = $this->m_manage_kerja->deleteKom($id_detail_kerja);
			if($delete){
				$result['status'] = "success";
				$date = date('Y-m-d');				
				$this->m_log->insertLog($_SESSION['id'],'Delete  Assessment',$date);
			}
		}
		echo json_encode($result);
	}
}
