<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Recruit extends CI_Controller {
	protected $statusLabel = ["Menunggu Persetujuan","Disetujui","Tidak Disetujui","Selesai"];
	function __construct(){
		parent::__construct();
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_recruit','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
	}
	public function index()
	{

		$idPekerjaan = $this->uri->segment(2);
		$temData = $this->m_client->getDetailClient($idPekerjaan);
		$data["id_client"] = $idPekerjaan;
		$data["namaKlien"] = $temData[0]->nama_client;
		$data['client'] = $temData[0];
		$this->load->view('Back_office/static/header',$data);
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/recruit');
		$this->load->view('Back_office/static/footer');
		if(!isset($_SESSION['nama'])){
			redirect(base_url().'login');
		}
	}

	function list()	{
		if(isset($_SESSION['id'])){
			$id_client = $this->uri->segment(3);
			$data = $this->m_recruit->list($id_client);
			foreach ($data as $key) {
				$time = strtotime($key->created_at);
				$key->created_at = date('m-d-Y',$time);
				$time = strtotime($key->deadline);
				$key->deadline = date('m/d/Y',$time);					
				$key->statusLabel = $this->statusLabel[$key->status];		
			}
				echo json_encode($data);			
		}
	}
	function change(){
		$result['status'] = 'failed';
		if(isset($_SESSION['id'])){
			$id_request = $_POST['idDetail'];
			$status = $_POST['status'];
			$id_client = $_POST['id_client'];
			$change = $this->m_recruit->change($id_request,$status);
			if($change){
				$result['status'] = 'success';
				if($status == 1){
					$pekerjaanKlien = $this->m_pekerjaan_client->getPekerjaanKlienByuser($id_client,1);
					 $idPekerjaan = $pekerjaanKlien[0]->id_daftar_pekerjaan_client;
					$created_at = date('Y-m-d h:i:s');
					$startDate = date('Y-m-d');
					$time = strtotime($_POST['deadline']);
					$deadLine = date('Y-m-d',$time);
					$insert = $this->m_pekerjaan_client->insert($idPekerjaan,$startDate,$deadLine,$created_at,$id_request);
					$data = $this->m_pekerjaan_client->allBool(1);
					foreach ($data as $key) {
						$id_detail = $key->id_detail_pekerjaan;
						$this->m_pekerjaan_client->initializeBool($insert,$id_detail);
					}
				}	
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_SESSION['id'])){
			$id = $_POST['id'];
			$request = $this->m_recruit->detail($id);
			foreach ($request as $key) {

					$time = strtotime($key->created_at);
					$key->created_at = date('m-d-Y',$time);
					$time = strtotime($key->deadline);
					$key->deadline = date('m/d/Y',$time);					
					$key->statusLabel = $this->statusLabel[$key->status];
		
				
			}
			echo json_encode($request);
		}
	}
}