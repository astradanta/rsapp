<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Client extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_invoice','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_confirm','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{	
		$user = $this->m_user->allUser();
		$taxUser = array();
		foreach ($user as $key) {
			if($key->id_role > 3 ){
				array_push($taxUser, $key);
			}
		}
		$data['tax_user'] = $taxUser;
		$this->load->view('Back_office/static/header',$data);
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/client');
		$this->load->view('Back_office/static/footer');
	}
	function listClient(){
		$data = $this->m_client->getClientList();
		$result = array();
		foreach ($data as $key) {
			$tempData['id'] = $key->id;
			$tempData['nama'] = $key->nama;
			$tempData['image'] = base_url().$key->image;
			$tempData['count'] = $key->count;
			if($key->image == null)$tempData['image'] = base_url()."assets/img/account.png";
			array_push($result, $tempData);
		}
		echo json_encode($result);
	}
	function pekerjaanKlien(){
	if(isset($_POST['baselink'])){
			$id = $_POST['id'];
			$data = $this->m_pekerjaan_client->getPekerjaanKlien($id);
			echo(json_encode($data));
		}
	}
	function listPekerjaan(){
		$data = $this->m_master->getListPekerjaan();
		$result = array();
		foreach ($data as $key) {
			$tempData['id'] = $key->id_pekerjaan;
			$tempData['nama'] = $key->nama_pekerjaan;
			array_push($result, $tempData);
		}
		echo json_encode($result);		
	}
	function filterClient(){
		if(isset($_POST['id'])&&(isset($_POST['keyword']))){
			$id = $_POST['id'];
			$keyword = $_POST['keyword'];
			$data = $this->m_client->filterClient($id,$keyword);
			$result = array();
			foreach ($data as $key) {
				$tempData['id'] = $key->id;
				$tempData['nama'] = $key->nama;
				$tempData['list_kerja'] = substr($key->list_kerja, 1,strlen($key->list_kerja)-2) ;
				$tempData['countKerja'] = $key->count;
				array_push($result, $tempData);
			}
			echo json_encode($result);
		}
		
	}
	function savePekerjaan(){
		$result['status'] = 'failed';
		if(isset($_POST['access'])){
			$pekerjaan = $_POST['idPekerjaan'];
			$id = $_POST['idDetail'];
			$listPekerjaan = $this->m_master->getListPekerjaan();
			$edit = 0;
			foreach ($listPekerjaan as $key) {
				$edit += $this->m_client->editPekerjaanClient($_POST['idPekerjaanKlien'.$key->id_pekerjaan],$id,$key->id_pekerjaan,$_POST['pic'.$key->id_pekerjaan]);
			}
			if(sizeof($listPekerjaan)== $edit)			{
				$result['status'] = 'success';
			}
			
		}
		echo(json_encode($result));
	}
	function addClient(){
		$result['status'] = 'failed';
		if (isset($_POST['btn_save'])){
			$nama = $_POST['nama'];
			$jenis = $_POST['jenis'];
			$alamat = $_POST['alamat'];
			$telepon = $_POST['telepon'];
			$email = $_POST['email'];
			$deskripsi = $_POST['deskripsi'];
			$time = strtotime($_POST['startContract']);
			$startContract = date('Y-m-d',$time);
			$time = strtotime($_POST['endContract']);
			$endContract = date('Y-m-d',$time);
			$logo = '';
			if ($_FILES['logo']['name'] != ""){
				$logo = $this->uploadLogo($logo);
			}
			$insert = $this->m_client->insertClient($nama,$jenis,$alamat,$telepon,$email,$deskripsi,$startContract,$endContract,$logo);
			if($insert){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Tambah Client '.$nama,$date);
			}

		}
		echo json_encode($result);
	}

	function uploadLogo($url){
				$date = new DateTime();
				$config['file_name']          = $date->getTimestamp().random_string('alnum', 5);
                $config['upload_path']          = 'assets/img/client/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 2000;
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('logo'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
                return $url;
	}

	function detailClient(){
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$data = $this->m_client->getDetailClient($id);
			echo json_encode($data);
		}
	}
	function editClient(){
		$result['status'] = 'failed';
		if(isset($_POST['idClient'])){
			$idClient = $_POST['idClient'];
			$nama = $_POST['nama'];
			$jenis = $_POST['jenis'];
			$alamat = $_POST['alamat'];
			$telepon = $_POST['telepon'];
			$email = $_POST['email'];
			$deskripsi = $_POST['deskripsi'];
			$time = strtotime($_POST['startContract']);
			$startContract = date('Y-m-d',$time);
			$time = strtotime($_POST['endContract']);
			$endContract = date('Y-m-d',$time);
			$data = $this->m_client->getDetailClient($idClient);
			$oldLogo = $data[0]->logo_client;
			$idUser = $data[0]->id_user;
			$logo = $oldLogo;			
			if ($_FILES['logo']['name'] != ""){
				$logo = $this->uploadLogo($logo);
				if ($logo != $oldLogo){
					if(file_exists(base_url().$oldLogo)){
						unlink(base_url().$oldLogo);
					}
				}
			}
			$edit = $this->m_client->editClient($idClient,$idUser,$nama,$jenis,$alamat,$telepon,$email,$deskripsi,$startContract,$endContract,$logo);
			if ($edit){
				$result['status'] = 'success';
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah Client '.$nama,$date);
			}
		}
		echo json_encode($result);
	}

	function deleteClient(){
		$result["status"] = "failed";
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$data = $this->m_client->getDetailClient($id);
			$idUser = $data[0]->id_user;
			$delete = $this->m_user->deleteUser($idUser);
			if($delete){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus Client '.$data[0]->nama_client,$date);
			}
		}
		echo json_encode($result);
	}
	function countElement(){
		$clien = $this->m_client->getClientList();
		$countClient = sizeof($clien);
		$all = $this->m_master->countPekerjaan();
		$countKerjaAll = 0;
		$countDikerjakanAll = 0;
		$countKerjaTax = 0;
		$countDikerjakanTax = 0;
		$countKerjaFInance = 0;
		$countDikerjakanFinance = 0;		
		foreach ($all as $key ) {
			$countKerjaAll += $key->total_kerja;
			$countDikerjakanAll +=$key->total_dikerjakan;
			if($key->tipe == "Tax"){
				$countKerjaTax += $key->total_kerja;
				$countDikerjakanTax +=$key->total_dikerjakan;				
			}
			if($key->tipe == "Finance"){
				$countKerjaFInance += $key->total_kerja;
				$countDikerjakanFinance +=$key->total_dikerjakan;				
			}			
		}
		$persentaseAll = 0;
		if($countKerjaAll!=0){$persentaseAll = round($countDikerjakanAll/$countKerjaAll*100);}
		$persentaseTax = 0;
		if($countKerjaTax!=0){$persentaseTax = round($countDikerjakanTax/$countKerjaTax*100);}
		$persentaseFinance = 0;
		if($countKerjaFInance!=0){$persentaseFinance = round($countDikerjakanFinance/$countKerjaFInance*100);}
		$result['countAll'] = $countClient;
		$result['persentaseAll'] = $persentaseAll;
		$result['persentaseTax'] = $persentaseTax;
		$result['persentaseFinance'] = $persentaseFinance;
		echo json_encode($result);
	}
}