<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Pekerjaan extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_pekerjaan','',TRUE);
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);	
	}
	public function index()
	{
		$idPekerjaan = $this->uri->segment(2);
		$temData = $this->m_client->getDetailClient($idPekerjaan);
		$data["idPekerjaanKlien"] = $idPekerjaan;
		$data["namaKlien"] = $temData[0]->nama_client;
		$data['client'] = $temData[0];
		$pekerjaan = $this->m_master->getListPekerjaan();
		$data['pekerjaan'] = $pekerjaan;
		$user = $this->m_user->allUser();
		$taxUser = array();
		foreach ($user as $key) {
			if($key->id_role == 4 ){
				array_push($taxUser, $key);
			}
		}
		$data['tax_user'] = $taxUser;
		$this->load->view('Back_office/static/header',$data);
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/pekerjaan');
		$this->load->view('Back_office/static/footer');
	}
	function list(){
		if(isset($_SESSION['id'])){
			if(isset($_POST['id_client'])){
				$id_client = $_POST['id_client'];
				$data = $this->m_pekerjaan->pekerjaanClient($id_client);
				echo json_encode($data);
			}
		}
	}
	function add(){
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id_client = $_POST['id_client'];
			$id_user = $_POST['id_user'];
			$nama_kerja = $_POST['nama_kerja'];
			$id_pekerjaan = $_POST['jenis'];
			$time = strtotime($_POST['mulai']);
			$mulai = date('Y-m-d',$time);
			$time = strtotime($_POST['selesai']);
			$selesai = date('Y-m-d',$time);
			$deskripsi = $_POST['note'];
			$insert = $this->m_pekerjaan->insertKerja($id_client,$id_pekerjaan,$id_user,$nama_kerja,$mulai,$selesai,$deskripsi);
			if($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Tambah pekerjaan klien ',$date);
			}

		}
		echo json_encode($result);
	}
	function detail (){
		if(isset($_SESSION['id'])&& isset($_POST['id'])){
			$id = $_POST['id'];
			$data = $this->m_pekerjaan->detailKerja($id);
			foreach ($data as $key) {
				$time = strtotime($key->start_date);
				$key->start_date = date('m/d/Y',$time);
				$time = strtotime($key->deadline);
				$key->deadline = date('m/d/Y',$time);
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->updated_at = date('d-m-Y',$time);
				}
			}
			echo json_encode($data);
		}
	}
	function edit(){
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id_kerja = $_POST['id_kerja'];
			$id_daftar_pekerjaan_client = $_POST['id_daftar_pekerjaan_client'];
			$id_user = $_POST['id_user'];
			$nama_kerja = $_POST['nama_kerja'];
			$id_pekerjaan = $_POST['jenis'];
			$time = strtotime($_POST['mulai']);
			$mulai = date('Y-m-d',$time);
			$time = strtotime($_POST['selesai']);
			$selesai = date('Y-m-d',$time);
			$deskripsi = $_POST['note'];
			$insert = $this->m_pekerjaan->editKerja($id_kerja,$id_daftar_pekerjaan_client,$id_pekerjaan,$id_user,$nama_kerja,$mulai,$selesai,$deskripsi);
			if($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah pekerjaan klien ',$date);
			}

		}
		echo json_encode($result);		
	}
	function delete(){
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id_kerja = $_POST['id'];
			$data = $this->m_pekerjaan->detailKerja($id_kerja);
			$delete = $this->m_pekerjaan->deleteKerja($data[0]->id_daftar_pekerjaan_client,$id_kerja);
			if($delete){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Delete pekerjaan klien ',$date);
			}
		}
		echo json_encode($result);
	}
	function change(){
		$result['status'] = "failed";
		if(isset($_SESSION['id'])&& isset($_POST['id_kerja'])){
			$id_kerja = $_POST['id_kerja'];
			$status = $_POST['status'];
			$change = $this->m_pekerjaan->changeStatus($id_kerja,$status);
			if($change){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah status pekerjaan klien menjadi '.$status,$date);
			}
		}
		echo json_encode($result);
	}
}
