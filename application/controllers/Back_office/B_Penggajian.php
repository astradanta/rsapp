<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Penggajian extends CI_Controller {
	protected $statusLabel = ["Menunggu Persetujuan","Disetujui","Tidak Disetujui","Selesai"];
	function __construct(){
		parent::__construct();
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_penggajian','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_log','',TRUE);
	}
	public function index()
	{

		$idPekerjaan = $this->uri->segment(2);
		$temData = $this->m_client->getDetailClient($idPekerjaan);
		$data["id_client"] = $idPekerjaan;
		$data["namaKlien"] = $temData[0]->nama_client;
		$data['client'] = $temData[0];
		$this->load->view('Back_office/static/header',$data);
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/penggajian');
		$this->load->view('Back_office/static/footer');
		if(!isset($_SESSION['nama'])){
			redirect(base_url().'login');
		}
	}

	function list()	{
		if(isset($_SESSION['id'])){
			$id_client = $_POST['id_client'];
			$data = $this->m_penggajian->adminDataAbsensi($id_client);
			foreach ($data as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->tanggal = date('d-m-Y',$time);
				} else {
					$time = strtotime($key->created_at);
					$key->tanggal = date('d-m-Y',$time);
				}
				if($key->note == null){
					$key->note = "";
				}
			}
			echo(json_encode($data));			
		}
	}
	function change(){
		$result['status'] = 'failed';
		if(isset($_SESSION['id'])){
			$id_kerja = $_POST['id_kerja'];
			$status = $_POST["status"];
			$edit = $this->m_penggajian->changeAbsensiStatus($id_kerja,$status);
			if($edit){
				$result['status'] = "success";
			}
		}
		echo json_encode($result);
	}
	function picker(){
		if(isset($_SESSION['id'])){
			$id_client = $_POST['id_client'];
			$data = $this->m_penggajian->pickerSlipGaji($id_client);
			echo json_encode($data);
		}
	}
	function add(){
		$result['status'] = "failed";
		if(isset($_SESSION['id'])){
			$id_kerja = $_POST['id_kerja'];
			$catatan = $_POST['catatan'];
			$url = $_POST['url'];
			$insert = $this->m_penggajian->insertSlip($id_kerja,$catatan,$url);
			if($insert){
				$result['status'] = "success";
			}
		}
		echo(json_encode($result));
	}
	function listSlip(){
			$id_client = $_POST['id_client'];
			$data = $this->m_penggajian->adminDataSlip($id_client);
			foreach ($data as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->tanggal = date('d-m-Y',$time);
				} else {
					$time = strtotime($key->created_at);
					$key->tanggal = date('d-m-Y',$time);
				}
				if($key->note == null){
					$key->note = "";
				}
			}
			echo(json_encode($data));		
	}
	function delete(){
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id_kerja = $_POST['id'];
			$delete_kerja = $this->m_penggajian->deleteDetailSlip($id_kerja);
			if($delete_kerja){
				$result['status'] = "success";
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_SESSION['id'])){
			$id = $_POST['id_client'];
			$id_kerja = $_POST['id_kerja'];
			$data = $this->m_penggajian->userSlipDetail($id_kerja);
			echo json_encode($data);			
		}
	}
	function edit(){
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id_detail_kerja = $_POST['id_detail_kerja'];
			$catatan = $_POST['catatan'];
			$id_kerja = $_POST["id_kerja"];
			$url = $_POST['url'];
			$edit = $this->m_penggajian->userSlipEdit($id_detail_kerja,$id_kerja,$catatan,$url);
			if($edit){
				$result["status"] = "success";
			}
		}
		echo(json_encode($result));
	}
	function finish()	{
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id = $_POST['id_kerja'];
			$change = $this->m_pekerjaan_client->changeStatus($id);
			if($change){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Mengganti status pekerjaan menjadi selesai',$date);
			}
		}
		echo(json_encode($result));
	}
}