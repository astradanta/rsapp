<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_manage_kerja extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function getDataKerja($id_kerja){
			$sql = "select tb_kerja.*, tb_daftar_pekerjaan_client.id_pekerjaan,tb_client.id_client,tb_client.nama_client as 'nama_client', tb_user.nama as'pic' from tb_kerja inner join tb_daftar_pekerjaan_client on tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client = tb_kerja.id_daftar_pekerjaan_client inner join tb_client on tb_daftar_pekerjaan_client.id_client = tb_client.id_client inner join tb_user on tb_user.id_user = tb_daftar_pekerjaan_client.id_user where id_kerja = ".$id_kerja;
			$query = $this->db->query($sql);
			return $query->result();
		}
		function addAssessmentProject($id_kerja,$upload_file,$note,$id_sub_detail_pekerjaan,$date){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>1,"upload_file"=>$upload_file,"note"=>$note,"id_sub_detail_pekerjaan"=>$id_sub_detail_pekerjaan,"date"=>$date,"created_at"=>$created_at);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addKomProject($id_kerja,$upload_file,$note,$id_sub_detail_pekerjaan,$date){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>2,"upload_file"=>$upload_file,"note"=>$note,"id_sub_detail_pekerjaan"=>$id_sub_detail_pekerjaan,"date"=>$date,"created_at"=>$created_at);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addKomProduct($id_kerja,$upload_file,$note,$id_sub_detail_pekerjaan,$date){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>9,"upload_file"=>$upload_file,"note"=>$note,"id_sub_detail_pekerjaan"=>$id_sub_detail_pekerjaan,"date"=>$date,"created_at"=>$created_at);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addDeveloptProject($id_kerja,$note,$date,$deadline,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>3,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"deadline"=>$deadline,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addDeveloptProduct($id_kerja,$note,$date,$deadline,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>10,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"deadline"=>$deadline,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addInstallationProject($id_kerja,$note,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>4,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addInstallationProduct($id_kerja,$note,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>11,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addInputProduct($id_kerja,$note,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>13,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addTutorialProject($id_kerja,$note,$date,$nama,$upload_file,$document_1,$document_2){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>5,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama,"upload_file"=>$upload_file,"document_1"=>$document_1,"document_2"=>$document_2);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addTutorialProduct($id_kerja,$note,$date,$nama,$upload_file,$document_1,$document_2){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>12,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama,"upload_file"=>$upload_file,"document_1"=>$document_1,"document_2"=>$document_2);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addBugProject($id_kerja,$note,$id_sub_detail_pekerjaan,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>6,"note"=>$note,"id_sub_detail_pekerjaan"=>$id_sub_detail_pekerjaan,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addBugProduct($id_kerja,$note,$id_sub_detail_pekerjaan,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>14,"note"=>$note,"id_sub_detail_pekerjaan"=>$id_sub_detail_pekerjaan,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addSystemProject($id_kerja,$note,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>7,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addSystemProduct($id_kerja,$note,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>15,"note"=>$note,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addMaintenanceProject($id_kerja,$note,$id_sub_detail_pekerjaan,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>8,"note"=>$note,"id_sub_detail_pekerjaan"=>$id_sub_detail_pekerjaan,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}
		function addMaintenanceProduct($id_kerja,$note,$id_sub_detail_pekerjaan,$date,$nama){
					$created_at = date("Y-m-d h:i:s");
					$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>16,"note"=>$note,"id_sub_detail_pekerjaan"=>$id_sub_detail_pekerjaan,"date"=>$date,"created_at"=>$created_at,"nama"=>$nama);
					return $this->db->insert('tb_detail_kerja',$data);
		}		
		function listAssessment($id_kerja){
			$this->db->where('id_detail_pekerjaan',1);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listKom($id_kerja){
			$this->db->where('id_detail_pekerjaan',2);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listKomProduct($id_kerja){
			$this->db->where('id_detail_pekerjaan',9);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listDevelopt($id_kerja){
			$this->db->where('id_detail_pekerjaan',3);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listDeveloptProduck($id_kerja){
			$this->db->where('id_detail_pekerjaan',10);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listInstallation($id_kerja){
			$this->db->where('id_detail_pekerjaan',4);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listInstallationProduct($id_kerja){
			$this->db->where('id_detail_pekerjaan',11);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listTutorial($id_kerja){
			$this->db->where('id_detail_pekerjaan',5);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listTutorialProduct($id_kerja){
			$this->db->where('id_detail_pekerjaan',12);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listBug($id_kerja){
			$this->db->where('id_detail_pekerjaan',6);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listInputProduct($id_kerja){
			$this->db->where('id_detail_pekerjaan',13);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listSystem($id_kerja){
			$this->db->where('id_detail_pekerjaan',7);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listSystemProduct($id_kerja){
			$this->db->where('id_detail_pekerjaan',15);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listBugProduct($id_kerja){
			$this->db->where('id_detail_pekerjaan',14);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listMaintance($id_kerja){
			$this->db->where('id_detail_pekerjaan',8);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function listMaintanceProduct($id_kerja){
			$this->db->where('id_detail_pekerjaan',16);
			$this->db->where('id_kerja',$id_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}				
		function detailAssessment($id_detail_kerja){
			$this->db->where('id_detail_kerja',$id_detail_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function detailKom($id_detail_kerja){
			$this->db->where('id_detail_kerja',$id_detail_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}
		function detailDevelopt($id_detail_kerja){
			$this->db->where('id_detail_kerja',$id_detail_kerja);
			return $this->db->get('tb_detail_kerja')->result();
		}		
		function editAssessmentProject($id_detail_kerja,$upload_file,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>1,"upload_file"=>$upload_file,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editKomProject($id_detail_kerja,$upload_file,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>2,"upload_file"=>$upload_file,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editKomProduct($id_detail_kerja,$upload_file,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>9,"upload_file"=>$upload_file,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editDevelopttProject($id_detail_kerja,$note,$date,$deadline,$nama){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>3,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at,"deadline"=>$deadline,"nama"=>$nama);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editDevelopttProduct($id_detail_kerja,$note,$date,$deadline,$nama){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>10,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at,"deadline"=>$deadline,"nama"=>$nama);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editInstallationProject($id_detail_kerja,$note,$date,$nama){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>4,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at,"nama"=>$nama);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editInstallationProduct($id_detail_kerja,$note,$date,$nama){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>11,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at,"nama"=>$nama);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editTutorialProject($id_detail_kerja,$note,$date,$nama,$upload_file,$document_1,$document_2){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>5,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at,"nama"=>$nama,"upload_file"=>$upload_file,"document_1"=>$document_1,"document_2"=>$document_2);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editTutorialProduct($id_detail_kerja,$note,$date,$nama,$upload_file,$document_1,$document_2){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>12,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at,"nama"=>$nama,"upload_file"=>$upload_file,"document_1"=>$document_1,"document_2"=>$document_2);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editBugProject($id_detail_kerja,$nama,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>6,"nama"=>$nama,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editBugProduct($id_detail_kerja,$nama,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>14,"nama"=>$nama,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editInputProduct($id_detail_kerja,$note,$date,$nama){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>13,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at,"nama"=>$nama);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editSystemProject($id_detail_kerja,$nama,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>7,"nama"=>$nama,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editSystemProduct($id_detail_kerja,$nama,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>15,"nama"=>$nama,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editMaintenanceProject($id_detail_kerja,$nama,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>8,"nama"=>$nama,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}
		function editMaintenanceProduct($id_detail_kerja,$nama,$note,$date){
				$updated_at = date("Y-m-d h:i:s");
				$data = array("id_detail_pekerjaan"=>16,"nama"=>$nama,"note"=>$note,"date"=>$date,"updated_at"=>$updated_at);
				$this->db->where('id_detail_kerja',$id_detail_kerja);
				return $this->db->update('tb_detail_kerja',$data);			
		}		
		function deleteAssessment($id_detail_kerja){
			$this->db->where('id_detail_kerja',$id_detail_kerja);
			return $this->db->delete("tb_detail_kerja");
		}
		function deleteKom($id_detail_kerja){
			$this->db->where('id_detail_kerja',$id_detail_kerja);
			return $this->db->delete("tb_detail_kerja");
		}
		function changeDevelopt($id_detail_kerja,$status){
			$this->db->where('id_detail_kerja',$id_detail_kerja);
			$data = array("status"=>$status);
			return $this->db->update("tb_detail_kerja",$data);
		}
	}
?>