<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_saran extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function list(){
			$this->db->select('tb_saran.*,tb_client.nama_client');
			$this->db->from('tb_saran');
			$this->db->join('tb_client','tb_saran.id_client = tb_client.id_client');
			return $this->db->get()->result();
		}
		function delete($id){
			$this->db->where('id_saran',$id);
			return $this->db->delete('tb_saran');
		}
		function saranClient($idClient){
			$this->db->where('id_client',$idClient);
			return $this->db->get('tb_saran')->result();
		}
		function edit($id,$content,$update_at){
			$data = array("content"=>$content,"update_at"=>$update_at);
			$this->db->where('id_saran',$id);
			return $this->db->update('tb_saran',$data);
		}
		function add($id_client,$content,$create_at){
			$data = array("content"=>$content,"create_at"=>$create_at,"id_client"=>$id_client);
			return $this->db->insert('tb_saran',$data);
		}
	}
?>