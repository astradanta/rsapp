<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_pekerjaan extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function pekerjaanClient($id){
			$sql = 'SELECT tb_kerja.*,tb_daftar_pekerjaan.id_pekerjaan,tb_daftar_pekerjaan.nama_pekerjaan as "jenis",tb_user.id_user, tb_user.nama as "pic" FROM tb_kerja INNER JOIN tb_daftar_pekerjaan_client on tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client = tb_kerja.id_daftar_pekerjaan_client INNER JOIN tb_daftar_pekerjaan on tb_daftar_pekerjaan_client.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan INNER JOIN tb_user on tb_daftar_pekerjaan_client.id_user = tb_user.id_user  WHERE tb_daftar_pekerjaan_client.id_client = '.$id;
			$query = $this->db->query($sql);
			return $query->result();
		}
		function insertPekerjaanKlien($id_client,$id_pekerjaan,$id_user){
			$created_at = date("Y-m-d H:i:s");
			$data = array("id_client"=>$id_client,"id_pekerjaan"=>$id_pekerjaan,"id_user"=>$id_user,"created_at"=>$created_at);
			$insert = $this->db->insert('tb_daftar_pekerjaan_client',$data);
			if (!$insert){
				return  null;
			} else {
				return $this->db->insert_id();
			}
		}
		function editPekerjaanKlien($id_daftar_pekerjaan_client,$id_pekerjaan,$id_user){
			$updated_at = date("Y-m-d H:i:s");
			$data = array("id_pekerjaan"=>$id_pekerjaan,"id_user"=>$id_user,"updated_at"=>$updated_at);
			$this->db->where('id_daftar_pekerjaan_client',$id_daftar_pekerjaan_client);
			$update = $this->db->update('tb_daftar_pekerjaan_client',$data);
			return $update;
		}
		function insertKerja($id_client,$id_pekerjaan,$id_user,$nama_kerja,$start_date,$deadline,$deskripsi){
			$initialize = $this->insertPekerjaanKlien($id_client,$id_pekerjaan,$id_user);
			if($initialize == null){
				return false;
			} else {
				$created_at = date("Y-m-d H:i:s");
				$id_daftar_pekerjaan_client = $initialize;
				$data = array("id_daftar_pekerjaan_client"=>$id_daftar_pekerjaan_client,"nama_kerja"=>$nama_kerja,"start_date"=>$start_date,"deadline"=>$deadline,"deskripsi"=>$deskripsi,"created_at"=>$created_at,"status"=>"on progress");
				return $this->db->insert('tb_kerja',$data);
			}
		}
		function editKerja($id_kerja,$id_daftar_pekerjaan_client,$id_pekerjaan,$id_user,$nama_kerja,$start_date,$deadline,$deskripsi){
			$initialize = $this->editPekerjaanKlien($id_daftar_pekerjaan_client,$id_pekerjaan,$id_user);
			if(!$initialize){
				return false;
			} else {
				$updated_at = date("Y-m-d H:i:s");
				$data = array("id_daftar_pekerjaan_client"=>$id_daftar_pekerjaan_client,"nama_kerja"=>$nama_kerja,"start_date"=>$start_date,"deadline"=>$deadline,"deskripsi"=>$deskripsi,"updated_at"=>$updated_at,"status"=>"on progress");
				$this->db->where("id_kerja",$id_kerja);
				return $this->db->update('tb_kerja',$data);
			}
		}
		function detailKerja($id){
			$sql = 'SELECT tb_kerja.*,tb_daftar_pekerjaan.id_pekerjaan,tb_daftar_pekerjaan.nama_pekerjaan as "jenis",tb_user.id_user, tb_user.nama as "pic" FROM tb_kerja INNER JOIN tb_daftar_pekerjaan_client on tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client = tb_kerja.id_daftar_pekerjaan_client INNER JOIN tb_daftar_pekerjaan on tb_daftar_pekerjaan_client.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan INNER JOIN tb_user on tb_daftar_pekerjaan_client.id_user = tb_user.id_user  WHERE tb_kerja.id_kerja = '.$id;
			$query = $this->db->query($sql);
			return $query->result();			
		}
		function deleteKerja($id_daftar_pekerjaan_client,$id_kerja){
			$deletePekerjaan = $this->deletePekerjaan($id_daftar_pekerjaan_client);
			if($deletePekerjaan){
				$this->db->where('id_kerja',$id_kerja);
				return $this->db->delete('tb_kerja');
			}
		}
		function deletePekerjaan($id_daftar_pekerjaan_client){
			$this->db->where('id_daftar_pekerjaan_client',$id_daftar_pekerjaan_client);
			return $this->db->delete('tb_daftar_pekerjaan_client');
		}
		function changeStatus($id_kerja,$status){
			$updated_at = date("Y-m-d h:i:s");
			$data = array("status"=>$status);
			$this->db->where("id_kerja",$id_kerja);
			return $this->db->update("tb_kerja",$data);
		}
	}
?>