<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Konfirmasi Pembayaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>client"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Konfirmasi Pembayaran</li>
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12" style="padding-top: 20px">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Konfirmasi Pembayaran</a></li>
              <li><a href="#tab_2" data-toggle="tab">Receipt</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1" >
                     <button type="button" class="btn btn-success pull-right" id="btnAddInvoice" data-toggle="modal" data-target="#manipulateModal" style="margin-bottom: 20px"><i class="fa fa-check"></i>&nbsp;Confirm</button>
                  <div class="row" id="contentInvoice">
                       <div class="col-md-12">
                         <table id="tableInvoice" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th width="15%">No Invoice</th>
                            <th width="15%">Nama Admin</th>
                            <th width="15%">Nama Bank</th>
                            <th width="10%">Atas Nama</th>
                            <th width="10%">Status</th>
                            <th width="20%">Catatan</th>                            
                            <th width="130">Aksi</th>
                          </tr>
                          </thead>
                          <tbody id="listView">

                          </tbody>
                        </table>                       
                      </div>
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <div class="row" id="contentArchive">
                       <div class="col-md-12">
                         <table id="tableReceipt" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th width="15%">Dibuat Oleh</th>
                            <th width="15%">No Invoice</th>
                            <th width="30%">Deskripsi</th>
                            <th width="15%">Dibuat Pada</th>
                            <th width="15%">Diubah Pada</th>                           
                            <th width="70">Aksi</th>
                          </tr>
                          </thead>
                          <tbody id="listViewReceipt">

                          </tbody>
                        </table>                       
                      </div> 
                  </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>          
        </div>
      </div>
    </section>
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Konfirmasi Pembayaran</h3>
              </div>
              <form action="<?php echo(base_url()) ?>client/confirm/add" method="post" id="manipulateForm" enctype="multipart/form-data">
                <div class="box-body">
                  <input type="hidden" id="idDetail" name="idDetail" value="">
                  <input type="hidden" name="access" value="1">
                  <div class="form-group">
                    <label>Invoice</label>
                    <select class="form-control select2" id="selectPicker" name="pekerjaan" style="width: 100%;">
                      <option value="0">Pilih invoice</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Bank</label>
                    <input type="text" class="form-control" name="namaBank" id="inputNamaBank" placeholder="" required="" autocomplete="off">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Atas Nama Bank</label>
                    <input type="text" class="form-control" name="atasNamaBank" id="inputAtasNamaBank" placeholder="" required="" autocomplete="off">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Catatan</label>
                    <textarea id="inputNote" name="note" class="form-control"></textarea>
                  </div>                  
                  <div class="form-group">
                    <label for="exampleInputEmail1">Bukti Transaksi</label>
                    <input type="file" class="form-control" name="bukti" id="inputBukti">
                  </div>                                                                                             
                  <input type="hidden" name="idPekerjaan" id="editIdPekerjaan" value="">
                  <input type="hidden" name="access" value="1">                                 
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>        