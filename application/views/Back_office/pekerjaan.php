<div class="content-wrapper">
	<section class="content-header">
      <h1>
        Daftar Pekerjaan Klien
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Daftar Pekerjaan Klien</li>
        <li class="active"><?php echo $namaKlien; ?></li>
        <input type="hidden" name="" id="idPekerjaanKlien" value="<?php echo($idPekerjaanKlien) ?>">
      </ol>
      <div class="col-xs-12 contentHeader"></div>
    </section>
    <section class="content">
		<div class="row">
	        <div class="col-xs-12">
	          
	          <!-- /.box -->

	          <div class="box box-danger timbul">
	            <div class="box-header">
	              <h3 class="box-title">Daftar Pekerjaan</h3>
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
	            	<div class="col-xs-12">
	            		<div class="form-group col-md-3">
			                <select class="form-control select2" id="selectJenis" name="" style="width: 100%;">
			                	<option value="-">Jenis Pekerjaan</option>
			                	<?php 
		                		
		                		foreach ($pekerjaan as $key) {
		                		?>
		                		<option value="<?php echo($key->nama_pekerjaan) ?>"><?php echo($key->nama_pekerjaan) ?></option>	
		                		<?php } ?>
			                </select>	            			
	            		</div>
	            		<button class="btn btn-success pull-right" type="button" id="addBtn" data-target="#manipulateModal" data-toggle="modal"><i class="fa fa-plus"></i>&nbsp;Tambah</button>	
	               </div>	
	              <table id="tablePekerjaan" class="table table-bordered table-striped">
	                <thead>
	                <tr>
	                  <th width="10%">No</th>
	                  <th width="20%">Nama Pekerjaan</th>
	                  <th width="10%">Jenis</th>
	                  <th width="10%">PIC</th>
	                  <th width="10%">Mulai</th>
	                  <th width="10%">Selesai</th>
	                  <th width="10%">Status</th>
	                  <th width="78">Aksi</th>
	                </tr>
	                </thead>
	                <tbody id="listView">
	               
	                </tbody>
	              </table>
	            </div>
	            <!-- /.box-body -->
	          </div>
	          <!-- /.box -->
	        </div>
	        <!-- /.col -->
      	</div>    	
    </section>
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title" id="modalTitle">Tambah Data Pekerjaan</h3>
	            </div>
	            <form action="<?php echo(base_url()) ?>pekerjaan/add" method="post" id="manipulateForm" enctype="multipart/form-data">
	              <div class="box-body">
	              	<input type="hidden" id="idDetail" name="idDetail" value="">
	                <div class="form-group">
	                  	<label for="">Nama Pekerjaan</label>
	               		<input type="text" class="form-control" name="nama_kerja" id="nama_kerja" placeholder="" required="" autocomplete="off">
	               		<input type="hidden" name="id_client" id="id_client" value="<?php echo($idPekerjaanKlien) ?>">
	               		<input type="hidden" name="id_kerja" id="id_kerja" value="">	                  
	               		<input type="hidden" name="id_daftar_pekerjaan_client" id="id_daftar_pekerjaan_client" value="">	                  
	                </div>
		              <div class="form-group">
		                <label>Jenis</label>
		                <div class="radio" style="padding-left: 20px">
		                	<?php 
		                		$i = 1;
		                		foreach ($pekerjaan as $key) {
		                	?>
		                		<label class="radio-inline"><input type="radio" id="radio<?php echo($i); ?>" name="jenis" value="<?php echo $key->id_pekerjaan ?>"><?php echo $key->nama_pekerjaan; ?></label>
		                	<?php $i++; } ?>
                      	</div>  
		              </div>
	                <div class="form-group">
	                  <label for="exampleInputEmail1">PIC</label>
	                  <select class="form-control select2" id="selectPIC" name="id_user" style="width: 100%;">
			              	<option value="0">Pilih PIC</option>
			              	<?php foreach ($tax_user as $key ) {
			              		?>
			              		<option value="<?php echo($key->id_user) ?>"><?php echo $key->nama; ?> </option>
			              		<?php
			              	} ?>
			          </select>
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Tanggal Mulai</label>
	                  <input type="text" class="form-control" name="mulai" id="mulai" placeholder="" required="" autocomplete="off">
	                </div>
	               	<div class="form-group">
	                  <label for="exampleInputEmail1">Tanggal Selesai</label>
	                  <input type="text" class="form-control" name="selesai" id="selesai" placeholder="" required="" autocomplete="off">
	                </div>		                		                		              	                
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Catatan</label>
	                  <textarea id="note" name="note" class="form-control"></textarea>
	                </div>           	                		                             	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	                <button type="submit" class="btn btn-success pull-right">Simpan</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="detailModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
              </div>
              <form action="" method="post" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Pekerjaan</label>
                    <input type="text" class="form-control" name="nama" id="detailPekerjaan" placeholder="Ketikan Nama" required="" disabled="">
                  </div>
                  <div class="form-group">
                    <label>Jenis </label>
                     <input type="text" class="form-control" name="nama" id="detailJenis" placeholder="Ketikan Nama" required="" disabled=""> 
                  </div> 
                  <div class="form-group">
                    <label>PIC </label>
            <input type="text" class="form-control" name="nama" id="detailPIC" placeholder="Ketikan Nama" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Mulai</label>
            <input type="text" class="form-control" name="nama" id="detailMulai" placeholder="" required="" disabled="">  
                  </div>
                  <div class="form-group">
                    <label>Selesai</label>
            <input type="text" class="form-control" name="nama" id="detailSelesai" placeholder="" required="" disabled="">  
                  </div>                                                                                                                             
                  <div class="form-group">
                    <label for="exampleInputPassword1">Deskripsi</label>
                    <div class="custom-form-control" id="detailSyarat"></div>
                  </div>
                  <div class="form-group">
                    <label>Status </label>
                    <input type="text" class="form-control" name="nama" id="detailStatus" placeholder="" required="" disabled="">  
                  </div>  
                  <div class="form-group">
                    <label>Dibuat Pada </label>
            <input type="text" class="form-control" name="nama" id="detailCreateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Pada </label>
            <input type="text" class="form-control" name="nama" id="detailUpdateAt" placeholder="" required="" disabled="">  
                  </div>                  
                  <input type="hidden" name="access" value="1">
                  <div class="form-group">
                     <label id="editGenerate"></label>                    
                  </div>                                    
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div> 
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="modal_change">
  <form action="<?php echo base_url(); ?>pekerjaan/change" method="post" id="changeForm">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Ubah Status Pekerjaan</h3>
              </div>
                <div class="box-body">
                  <input type="hidden" id="id_kerja_change" name="id_kerja" value="">
                  <div class="form-group">
                    <label>Status Pekerjaan</label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="radio_onprogress" name="status" value="on progress">On Progress</label>
                          <label class="radio-inline"><input type="radio" id="radio_selesai" name="status" value="selesai">Selesai</label>
                          <label class="radio-inline"><input type="radio" id="radio_pending" name="status" value="pending">Pending</label>
                      </div>  

                  </div>                               
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" id="changeButton" class="btn btn-success pull-right">Simpan</button>
                </div>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
          </form>
        </div>        