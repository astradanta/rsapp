<div class="col-md-12 topbarPekerjaan">
          <ul class="topbarPekerjaan-menu" data-widget="tree">
            <li class="treeview">
              <a href="#">
                <i class=""></i>
                <span>WORKING NAVIGATION</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if($this->uri->segment(1) != "client") { ?>
                <li class="<?php if($this->uri->segment(1)=="assessment") echo 'active'; ?>"><a href="<?php echo(base_url().'assessment/'.$this->uri->segment(2)); ?>"><span>Assesment</span></a></li>
                <li class="<?php if($this->uri->segment(1)=="kom") echo 'active'; ?>"><a href="<?php echo(base_url().'kom/'.$this->uri->segment(2)); ?>"><span>Kick Of Meeting</span></a></li>
                <li class="<?php if($this->uri->segment(1)=="developt") echo 'active'; ?>"><a href="<?php echo(base_url().'developt/'.$this->uri->segment(2)); ?>"><span>Development Process</span></a></li>
                <li class="<?php if($this->uri->segment(1)=="installation") echo 'active'; ?>"><a href="<?php echo(base_url().'installation/'.$this->uri->segment(2)); ?>"><span>System Instalation</span></a></li>
                <li class="<?php if($this->uri->segment(1)=="tutorial") echo 'active'; ?>"><a href="<?php echo(base_url().'tutorial/'.$this->uri->segment(2)); ?>"><span>Tutorial Training</span></a></li>
                <li class="<?php if($this->uri->segment(1)=="bug") echo 'active'; ?>"><a href="<?php echo(base_url().'bug/'.$this->uri->segment(2)); ?>"><span>Bugs Fixing</span></a></li>
                <li class="<?php if($this->uri->segment(1)=="systemRunning") echo 'active'; ?>"><a href="<?php echo(base_url().'systemRunning/'.$this->uri->segment(2)); ?>"><span>System Running</span></a></li>
                <li class="<?php if($this->uri->segment(1)=="maintenance") echo 'active'; ?>"><a href="<?php echo(base_url().'maintenance/'.$this->uri->segment(2)); ?>"><span>Maintenance</span></a></li>
                <?php } else { ?>
                 <li class="<?php if($this->uri->segment(2)=="assessment") echo 'active'; ?>"><a href="<?php echo(base_url().'client/assessment/'.$this->uri->segment(3)); ?>"><span>Assesment</span></a></li>
                <li class="<?php if($this->uri->segment(2)=="kom") echo 'active'; ?>"><a href="<?php echo(base_url().'client/kom/'.$this->uri->segment(3)); ?>"><span>Kick Of Meeting</span></a></li>
                <li class="<?php if($this->uri->segment(2)=="developt") echo 'active'; ?>"><a href="<?php echo(base_url().'client/developt/'.$this->uri->segment(3)); ?>"><span>Development Process</span></a></li>
                <li class="<?php if($this->uri->segment(2)=="installation") echo 'active'; ?>"><a href="<?php echo(base_url().'client/installation/'.$this->uri->segment(3)); ?>"><span>System Instalation</span></a></li>
                <li class="<?php if($this->uri->segment(2)=="tutorial") echo 'active'; ?>"><a href="<?php echo(base_url().'client/tutorial/'.$this->uri->segment(3)); ?>"><span>Tutorial Training</span></a></li>
                <li class="<?php if($this->uri->segment(2)=="bug") echo 'active'; ?>"><a href="<?php echo(base_url().'client/bug/'.$this->uri->segment(3)); ?>"><span>Bugs Fixing</span></a></li>
                <li class="<?php if($this->uri->segment(2)=="systemRunning") echo 'active'; ?>"><a href="<?php echo(base_url().'client/systemRunning/'.$this->uri->segment(3)); ?>"><span>System Running</span></a></li>
                <li class="<?php if($this->uri->segment(2)=="maintenance") echo 'active'; ?>"><a href="<?php echo(base_url().'client/maintenance/'.$this->uri->segment(3)); ?>"><span>Maintenance</span></a></li>
                <?php } ?>       
              </ul>
            </li>
          </ul>          
        </div>
        <div class="col-lg-2 col-md-3 col-sm-4 sidebarPekerjaan">
          <ul class="sidebarPekerjaan-menu" data-widget="tree">
            <li class="header">WORKING NAVIGATION</li>
            <?php if($this->uri->segment(1) != "client") { ?>
            <li class="<?php if($this->uri->segment(1)=="assessment") echo 'active'; ?>"><a href="<?php echo(base_url().'assessment/'.$this->uri->segment(2)); ?>"><span>Assesment</span></a></li>
            <li class="<?php if($this->uri->segment(1)=="kom") echo 'active'; ?>"><a href="<?php echo(base_url().'kom/'.$this->uri->segment(2)); ?>"><span>Kick Of Meeting</span></a></li>
            <li class="<?php if($this->uri->segment(1)=="developt") echo 'active'; ?>"><a href="<?php echo(base_url().'developt/'.$this->uri->segment(2)); ?>"><span>Development Process</span></a></li>
            <li class="<?php if($this->uri->segment(1)=="installation") echo 'active'; ?>"><a href="<?php echo(base_url().'installation/'.$this->uri->segment(2)); ?>"><span>System Instalation</span></a></li>
            <li class="<?php if($this->uri->segment(1)=="tutorial") echo 'active'; ?>"><a href="<?php echo(base_url().'tutorial/'.$this->uri->segment(2)); ?>"><span>Tutorial Training</span></a></li>
            <li class="<?php if($this->uri->segment(1)=="bug") echo 'active'; ?>"><a href="<?php echo(base_url().'bug/'.$this->uri->segment(2)); ?>"><span>Bugs Fixing</span></a></li>
            <li class="<?php if($this->uri->segment(1)=="systemRunning") echo 'active'; ?>"><a href="<?php echo(base_url().'systemRunning/'.$this->uri->segment(2)); ?>"><span>System Running</span></a></li>
            <li class="<?php if($this->uri->segment(1)=="maintenance") echo 'active'; ?>"><a href="<?php echo(base_url().'maintenance/'.$this->uri->segment(2)); ?>"><span>Maintenance</span></a></li>
            <?php } else { ?>
             <li class="<?php if($this->uri->segment(2)=="assessment") echo 'active'; ?>"><a href="<?php echo(base_url().'client/assessment/'.$this->uri->segment(3)); ?>"><span>Assesment</span></a></li>
            <li class="<?php if($this->uri->segment(2)=="kom") echo 'active'; ?>"><a href="<?php echo(base_url().'client/kom/'.$this->uri->segment(3)); ?>"><span>Kick Of Meeting</span></a></li>
            <li class="<?php if($this->uri->segment(2)=="developt") echo 'active'; ?>"><a href="<?php echo(base_url().'client/developt/'.$this->uri->segment(3)); ?>"><span>Development Process</span></a></li>
            <li class="<?php if($this->uri->segment(2)=="installation") echo 'active'; ?>"><a href="<?php echo(base_url().'client/installation/'.$this->uri->segment(3)); ?>"><span>System Instalation</span></a></li>
            <li class="<?php if($this->uri->segment(2)=="tutorial") echo 'active'; ?>"><a href="<?php echo(base_url().'client/tutorial/'.$this->uri->segment(3)); ?>"><span>Tutorial Training</span></a></li>
            <li class="<?php if($this->uri->segment(2)=="bug") echo 'active'; ?>"><a href="<?php echo(base_url().'client/bug/'.$this->uri->segment(3)); ?>"><span>Bugs Fixing</span></a></li>
            <li class="<?php if($this->uri->segment(2)=="systemRunning") echo 'active'; ?>"><a href="<?php echo(base_url().'client/systemRunning/'.$this->uri->segment(3)); ?>"><span>System Running</span></a></li>
            <li class="<?php if($this->uri->segment(2)=="maintenance") echo 'active'; ?>"><a href="<?php echo(base_url().'client/maintenance/'.$this->uri->segment(3)); ?>"><span>Maintenance</span></a></li>
            <?php } ?>
          </ul>          
        </div>