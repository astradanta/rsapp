<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">
  <input type="hidden" name="" id="baselink" value="<?php echo(base_url()); ?>">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="navbar-header">
          <a href="<?php echo base_url()?>" class="navbar-brand"><b>RED</b> System</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Notifications Menu -->
             <li><a href="<?php echo(base_url())?>log">Log Activity</a></li>
            <li class="dropdown notifications-menu">
              <!-- Menu toggle button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- Inner Menu: contains the notifications -->
                  <ul class="menu">
                    <li><!-- start notification -->
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                    <!-- end notification -->
                  </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
              </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="<?php echo base_url()?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"><?php echo $_SESSION['nama']; ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="<?php echo base_url()?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                    <?php  ?>
                    <small><?php var_dump($_SESSION)?></small>
                  </p>
                </li>
                
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-right">
                    <button  class="btn btn-default btn-flat" id="signout">Sign out</button>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="clearfix"></div>
        <div class="navMenu">
          <div class="container">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="<?php echo(base_url())?>home">Home</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master Data <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                      <?php if($_SESSION['id']!=1) { ?>
                    <li><a href="<?php echo(base_url()) ?>user">User</a></li>
                    <?php } ?>
                    <li><a href="<?php echo(base_url()) ?>pekerjaan">Daftar Pekerjaan</a></li>
                    <li><a href="<?php echo(base_url()) ?>detailPekerjaan">Daftar Detail Pekerjaan</a></li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Payment<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="<?php echo base_url(); ?>invoice">Invoice</a></li>
                    <li><a href="<?php echo base_url(); ?>confirm
                      ">Konfirmasi Pembayaran</a></li>
                    <li><a href="<?php echo base_url(); ?>receipt">Receipt</a></li>
                  </ul>
                </li>
                <li><a href="#">Company Profile</a></li>
                <li><a href="<?php echo base_url(); ?>saran"">Saran & Masukan</a></li>
                <li><a href="<?php echo base_url(); ?>historyPekerjaan">History Pekerjaan</a></li>
              </ul>
            </div>
          </div>
          <!-- /.container-fluid -->          
        </div>

    </nav>
  </header>