<div class="col-md-12 topbarPekerjaan">
          <ul class="topbarPekerjaan-menu" data-widget="tree">
            <li class="treeview">
              <a href="#">
                <i class=""></i>
                <span>WORKING NAVIGATION</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if($this->uri->segment(1) != "client") { ?>

        <li class="<?php if($this->uri->segment(1)=="kom_product") echo 'active'; ?>"><a href="<?php echo(base_url().'kom_product/'.$this->uri->segment(2)); ?>"><span>Kick Of Meeting</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="developt_product") echo 'active'; ?>"><a href="<?php echo(base_url().'developt_product/'.$this->uri->segment(2)); ?>"><span>Development Process</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="installation_product") echo 'active'; ?>"><a href="<?php echo(base_url().'installation_product/'.$this->uri->segment(2)); ?>"><span>System Instalation</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="tutorial_product") echo 'active'; ?>"><a href="<?php echo(base_url().'tutorial_product/'.$this->uri->segment(2)); ?>"><span>Tutorial Training</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="input_product") echo 'active'; ?>"><a href="<?php echo(base_url().'input_product/'.$this->uri->segment(2)); ?>"><span>Inputing Data</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="bug_product") echo 'active'; ?>"><a href="<?php echo(base_url().'bug_product/'.$this->uri->segment(2)); ?>"><span>Bugs Fixing</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="system_product") echo 'active'; ?>"><a href="<?php echo(base_url().'system_product/'.$this->uri->segment(2)); ?>"><span>System Running</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="maintenance_product") echo 'active'; ?>"><a href="<?php echo(base_url().'maintenance_product/'.$this->uri->segment(2)); ?>"><span>Maintenance</span></a></li>
        <?php } else { ?>
        <li class="<?php if($this->uri->segment(2)=="kom_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/kom_product/'.$this->uri->segment(3)); ?>"><span>Kick Of Meeting</span></a></li>
        <li class="<?php if($this->uri->segment(2)=="developt_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/developt_product/'.$this->uri->segment(3)); ?>"><span>Development Process</span></a></li>
        <li class="<?php if($this->uri->segment(2)=="installation_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/installation_product/'.$this->uri->segment(3)); ?>"><span>System Instalation</span></a></li>
        <li class="<?php if($this->uri->segment(2)=="tutorial_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/tutorial_product/'.$this->uri->segment(3)); ?>"><span>Tutorial Training</span></a></li>
        <li class="<?php if($this->uri->segment(2)=="input_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/input_product/'.$this->uri->segment(3)); ?>"><span>Inputing Data</span></a></li>
        <li class="<?php if($this->uri->segment(2)=="bug_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/bug_product/'.$this->uri->segment(3)); ?>"><span>Bugs Fixing</span></a></li>
        <li class="<?php if($this->uri->segment(2)=="system_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/system_product/'.$this->uri->segment(3)); ?>"><span>System Running</span></a></li>
        <li class="<?php if($this->uri->segment(2)=="maintenance_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/maintenance_product/'.$this->uri->segment(3)); ?>"><span>Maintenance</span></a></li>
        <?php }?>      
              </ul>
            </li>
          </ul>          
        </div>
        <div class="col-lg-2 col-md-3 col-sm-4 sidebarPekerjaan">
          <ul class="sidebarPekerjaan-menu" data-widget="tree">
            <li class="header">WORKING NAVIGATION</li>
             <?php if($this->uri->segment(1) != "client") { ?>

                    <li class="<?php if($this->uri->segment(1)=="kom_product") echo 'active'; ?>"><a href="<?php echo(base_url().'kom_product/'.$this->uri->segment(2)); ?>"><span>Kick Of Meeting</span></a></li>
                    <li class="<?php if($this->uri->segment(1)=="developt_product") echo 'active'; ?>"><a href="<?php echo(base_url().'developt_product/'.$this->uri->segment(2)); ?>"><span>Development Process</span></a></li>
                    <li class="<?php if($this->uri->segment(1)=="installation_product") echo 'active'; ?>"><a href="<?php echo(base_url().'installation_product/'.$this->uri->segment(2)); ?>"><span>System Instalation</span></a></li>
                    <li class="<?php if($this->uri->segment(1)=="tutorial_product") echo 'active'; ?>"><a href="<?php echo(base_url().'tutorial_product/'.$this->uri->segment(2)); ?>"><span>Tutorial Training</span></a></li>
                    <li class="<?php if($this->uri->segment(1)=="input_product") echo 'active'; ?>"><a href="<?php echo(base_url().'input_product/'.$this->uri->segment(2)); ?>"><span>Inputing Data</span></a></li>
                    <li class="<?php if($this->uri->segment(1)=="bug_product") echo 'active'; ?>"><a href="<?php echo(base_url().'bug_product/'.$this->uri->segment(2)); ?>"><span>Bugs Fixing</span></a></li>
                    <li class="<?php if($this->uri->segment(1)=="system_product") echo 'active'; ?>"><a href="<?php echo(base_url().'system_product/'.$this->uri->segment(2)); ?>"><span>System Running</span></a></li>
                    <li class="<?php if($this->uri->segment(1)=="maintenance_product") echo 'active'; ?>"><a href="<?php echo(base_url().'maintenance_product/'.$this->uri->segment(2)); ?>"><span>Maintenance</span></a></li>
                    <?php } else { ?>
                    <li class="<?php if($this->uri->segment(2)=="kom_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/kom_product/'.$this->uri->segment(3)); ?>"><span>Kick Of Meeting</span></a></li>
                    <li class="<?php if($this->uri->segment(2)=="developt_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/developt_product/'.$this->uri->segment(3)); ?>"><span>Development Process</span></a></li>
                    <li class="<?php if($this->uri->segment(2)=="installation_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/installation_product/'.$this->uri->segment(3)); ?>"><span>System Instalation</span></a></li>
                    <li class="<?php if($this->uri->segment(2)=="tutorial_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/tutorial_product/'.$this->uri->segment(3)); ?>"><span>Tutorial Training</span></a></li>
                    <li class="<?php if($this->uri->segment(2)=="input_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/input_product/'.$this->uri->segment(3)); ?>"><span>Inputing Data</span></a></li>
                    <li class="<?php if($this->uri->segment(2)=="bug_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/bug_product/'.$this->uri->segment(3)); ?>"><span>Bugs Fixing</span></a></li>
                    <li class="<?php if($this->uri->segment(2)=="system_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/system_product/'.$this->uri->segment(3)); ?>"><span>System Running</span></a></li>
                    <li class="<?php if($this->uri->segment(2)=="maintenance_product") echo 'active'; ?>"><a href="<?php echo(base_url().'client/maintenance_product/'.$this->uri->segment(3)); ?>"><span>Maintenance</span></a></li>
                    <?php }?>           
          </ul>          
        </div>
