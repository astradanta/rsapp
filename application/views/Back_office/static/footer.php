<?php if($this->uri->segment(1)!="client") { ?>
<div class="modal fade" id="modalProfile">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Form Ubah Profile</h3>
              </div>
              <form action="" method="post" id="editFormGlobal" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama User</label>
                    <input type="text" class="form-control" name="nama" id="editNameProfile" placeholder="Ketikan Nama" required="" value="<?php echo $_SESSION['nama']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Email User</label>
                    <input type="email" class="form-control" name="email" id="editEmailProfile" placeholder="Ketikan Email" required="" value="<?php echo $_SESSION['email']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Password User</label>
                    <input type="password" class="form-control" id="editPasswordProfile" placeholder="Password" name="password" required="" value="<?php echo $_SESSION['password']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Ulangi Password</label>
                    <input type="password" class="form-control" id="editRetypeProfile" placeholder="Ulangi password" required="" value="<?php echo $_SESSION['password']; ?>">
                  </div>
                  <input type="hidden" name="editJenis" value="<?php echo $_SESSION['role_id']; ?>">
                  <div class="form-group" >
                      <label for="exampleInputEmail1">Photo</label>
                      <div >
                        <input type="file" class="form-control" name="logo" id="editLogoProfile">
                      </div>
                    </div>  
                  <input type="hidden" name="idUser" id="editIdUserProfile" value="<?php echo $_SESSION['id']; ?>">
                  <input type="hidden" name="access" value="1">
                  <input type="hidden" name="global" value="1">
                  <div class="form-group">
                     <label id="editGenerate"></label>                    
                  </div>                                    
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="button" class="btn btn-default btnGenerate" data-target="editGenerate">Generate Password</button>
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<?php } ?>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Ganeshcom Studio</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url()?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.redirect.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>assets/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

  <script src="<?php echo base_url()?>assets/script/global.js"></script>
  <?php if($this->uri->segment(1) == 'log'){?>
  <script src="<?php echo base_url()?>assets/script/log.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'user'){?>
  <script src="<?php echo base_url()?>assets/script/user.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'manageClient'){?>
  <script src="<?php echo base_url()?>assets/script/client.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'manageKerja'){?>
  <script src="<?php echo base_url()?>assets/script/manageKerja.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'invoice'){?>
  <script src="<?php echo base_url()?>assets/script/invoice.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'confirm'){?>
  <script src="<?php echo base_url()?>assets/script/confirm.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'receipt'){?>
  <script src="<?php echo base_url()?>assets/script/receipt.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'saran'){?>
  <script src="<?php echo base_url()?>assets/script/saran.js"></script>
<?php } ?>

<?php if($this->uri->segment(1) == 'historyPekerjaan'){?>
  <script src="<?php echo base_url()?>assets/script/historyPekerjaan.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'pekerjaanKlien'){?>
  <script src="<?php echo base_url()?>assets/script/pekerjaanKlien.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'detailHistory'){?>
  <script src="<?php echo base_url()?>assets/script/detailHistory.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'assessment' || $this->uri->segment(2) == 'assessment'){?>
  <script src="<?php echo base_url()?>assets/script/assessment.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'kom' || $this->uri->segment(2) == 'kom'){?>
  <script src="<?php echo base_url()?>assets/script/kom.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'developt'|| $this->uri->segment(2) == 'developt'){?>
  <script src="<?php echo base_url()?>assets/script/developt.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'installation'|| $this->uri->segment(2) == 'installation'){?>
  <script src="<?php echo base_url()?>assets/script/installation.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'tutorial'|| $this->uri->segment(2) == 'tutorial'){?>
  <script src="<?php echo base_url()?>assets/script/tutorial.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'bug'|| $this->uri->segment(2) == 'bug'){?>
  <script src="<?php echo base_url()?>assets/script/bug.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'systemRunning'|| $this->uri->segment(2) == 'systemRunning'){?>
  <script src="<?php echo base_url()?>assets/script/system.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'maintenance'|| $this->uri->segment(2) == 'maintenance'){?>
  <script src="<?php echo base_url()?>assets/script/maintenance.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>


<?php if($this->uri->segment(1) == 'kom_product'|| $this->uri->segment(2) == 'kom_product'){?>
  <script src="<?php echo base_url()?>assets/script/kom_product.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'developt_product'|| $this->uri->segment(2) == 'developt_product'){?>
  <script src="<?php echo base_url()?>assets/script/developt_product.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'installation_product'|| $this->uri->segment(2) == 'installation_product'){?>
  <script src="<?php echo base_url()?>assets/script/installation_product.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'tutorial_product'|| $this->uri->segment(2) == 'tutorial_product'){?>
  <script src="<?php echo base_url()?>assets/script/tutorial_product.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'input_product'|| $this->uri->segment(2) == 'input_product'){?>
  <script src="<?php echo base_url()?>assets/script/input_product.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'bug_product'|| $this->uri->segment(2) == 'bug_product'){?>
  <script src="<?php echo base_url()?>assets/script/bug_product.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'system_product'|| $this->uri->segment(2) == 'system_product'){?>
  <script src="<?php echo base_url()?>assets/script/system_product.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>
<?php if($this->uri->segment(1) == 'maintenance_product'|| $this->uri->segment(2) == 'maintenance_product'){?>
  <script src="<?php echo base_url()?>assets/script/maintenance_product.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script
<?php } ?>




<?php if($this->uri->segment(1) == 'pekerjaan'){?>
  <script src="<?php echo base_url()?>assets/script/pekerjaan.js"></script>
  <script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'home'){?>
  <script src="<?php echo base_url()?>assets/dist/js/pages/dashboard.js"></script>
  <script src="<?php echo base_url()?>assets/script/home.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == ''){?>
  <script src="<?php echo base_url()?>assets/dist/js/pages/dashboard.js"></script>
  <script src="<?php echo base_url()?>assets/script/home.js"></script>
<?php } ?>

</body>
</html>