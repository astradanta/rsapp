  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box timbul">
            <div class="box-header">
              <h3 class="box-title">History Pekerjaan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-12" style="padding-right: 0px;">
                  <div class="form-group">
                      <div class="col-md-3">
                          <label>From Date</label>
                          <input type="text" name="" id="fromDate" class="form-control" autocomplete="off">
                       </div>
                      <div class="col-md-3">
                          <label>Until Date</label>
                          <input type="text" name="" id="untilDate" class="form-control" autocomplete="off"> 
                       </div>                       
                  </div>
                </div>
              </div>
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Klien</th>
                  <th>Nama Pekerjaan</th>
                  <th>Jenis Pekerjaan</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody id="listView">
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <div class="modal fade" id="listModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">List detail history</h3>
              </div>
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama Pekerjaan</th>
                        <th>Link</th>
                      </tr>
                    </thead>
                    <tbody id="modalList">
                     
                    </tbody>
                  </table>

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                </div>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div></div>