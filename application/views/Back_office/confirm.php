<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Konfirmasi Pembayaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Payment</li>
        <li class="active">Konfirmasi Pembayaran</li>
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
      	<div class="col-md-12" style="padding-top: 20px">
            <div class="col-md-12">
              <div class="box box-primary timbul">
                <div class="box-header">
                  <h3 class="box-title">Daftar Konfirmasi Pembayaran</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row" id="contentInvoice">
                       <div class="col-md-12">
                         <table id="tableInvoice" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th width="10%">No Invoice</th>
                            <th width="10%">Nama Klien</th>
                            <th width="10%">Nama Bank</th>
                            <th width="15%">Atas Nama</th>
                            <th width="15%">Status</th>
                            <th width="20%">Catatan</th>                            
                            <th width="150">Aksi</th>
                          </tr>
                          </thead>
                          <tbody id="listView">

                          </tbody>
                        </table>                       
                      </div>
                </div>
                <!-- /.box-body -->
              </div>                       
            </div>             		
      	</div>
      </div>
    </section>
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Ubah Status Konfirmasi</h3>
              </div>
                <div class="box-body">
                  <input type="hidden" id="idDetail" name="idDetail" value="">
                  <input type="hidden" name="access" value="1">
                  <div class="form-group">
                    <label>Status Konfirmasi</label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="firstRadio" name="statusKonfirmasi" value="Terkonfirmasi">Terkonfirmasi</label>
                          <label class="radio-inline"><input type="radio" id="secondRadio" name="statusKonfirmasi" value="Gagal">Gagal</label>
                      </div>  

                  </div>                               
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="button" id="changeButton" class="btn btn-success pull-right">Simpan</button>
                </div>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
