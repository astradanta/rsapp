<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Saran dan Masukan
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Saran dan Masukan</li>
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
      	<div class="col-md-12" style="padding-top: 20px">
            <div class="col-md-12">
              <div class="box box-primary timbul">
                <div class="box-header">
                  <h3 class="box-title">Daftar Saran dan Masukan Klien</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row" id="contentInvoice">
                       <div class="col-md-12">
                         <table id="tableInvoice" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th width="15%">Nama Client</th>
                            <th width="15%">Tanggal</th>
                            <th width="30%">Konten Saran</th>
                            <th width="20%">Diubah Pada</th>
                            <th width="20%">Aksi</th>
                          </tr>
                          </thead>
                          <tbody id="listView">

                          </tbody>
                        </table>                       
                      </div>
                </div>
                <!-- /.box-body -->
              </div>                       
            </div>             		
      	</div>
      </div>
    </section>
</div>
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
