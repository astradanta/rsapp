
<div class="content-wrapper">
	<section class="content-header">
      <h1>
        <?php echo $nama_client; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?php echo (base_url().'/pekerjaanKlien/'.$id_client)?>"><?php echo $nama_client; ?></a></li>
        <li class="active">Maintenance</li>
        <input type="hidden" name="id_client" id="id_client" value="<?php echo $id_client; ?>">
        <input type="hidden" name="id_pekerjaan" id="id_pekerjaan" value="<?php echo $id_pekerjaan; ?>">
        <input type="hidden" name="status_kerja" id="status_kerja" value="<?php echo $status_kerja; ?>">
      </ol>
      <div class="col-xs-12 contentHeader-red" style="margin-top: 10px;"></div>
    </section>
   <section class="content">
 <div class="row">
        <div class="col-md-12">
          <h4 class="box-title col-md-3"><?php echo ($nama_kerja); ?></h4>
          <div class="col-xs-12 contentHeader-red" style="margin-top: 10px;"></div>
        </div>
        
        <div class="col-md-12">
              <h4 class="box-title col-md-3">PIC : <?php echo($pic); ?></h4>
              <h4 class="box-title col-md-3" style="color: green" id="lblStartdate"> Start Date : <?php echo($start_date) ?></h4>
              <h4 class="box-title col-md-3" style="color: red" id="lblDeadline"> Deadline : <?php echo($deadline) ?></h4>
        </div>
      </div>
      <div class="row">
       <?php $this->load->view('Back_office/static/productSidebar'); ?>
        <div class="col-lg-10 col-md-9 col-sm-8" style="padding: 20px;">
          <!-- Custom Tabs -->
          <div class="box box-danger timbul">
            <!-- /.box-header -->
            <div class="box-body">                
                <div class="row" style="padding-top: 20px;">
                  <div class="col-md-12">
                          <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                              <li class="active" id="tabBar1"><a href="#tab_1" data-toggle="tab">Issue</a></li>
                              <li id="tabBar2" ><a href="#tab_2" data-toggle="tab">Fixing</a></li>
                            </ul>
                            <div class="tab-content">
                              <div class="tab-pane active" id="tab_1" style="padding-top: 10px;">
                                  <button class="btn btn-success pull-right" type="button" id="btnAddAbsen" style="margin-bottom: 10px;"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
                                  <div class="row" id="contentInvoice">
                                       <div class="col-md-12">
                                         <table id="tableAbsen" class="table table-bordered table-striped">
                                          <thead>
                                          <tr>
                                            <th width="25%">Nama Issue</th>
                                            <th width="25%">Tanggal</th>
                                            <th width="25%">Deskripsi</th>
                                            <th width="100">Aksi</th>
                                          </tr>
                                          </thead>
                                          <tbody id="listViewAbsen">

                                          </tbody>
                                        </table>                       
                                      </div>
                                  </div>
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_2">
                                <button class="btn btn-success pull-right" type="button" id="btnAddMom" style="margin-bottom: 10px;"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
                                  <div class="row" id="contentArchive" style="padding-top: 10px;">
                                  
                                       <div class="col-md-12">
                                         <table id="tableMom" class="table table-bordered table-striped">
                                          <thead>
                                          <tr>
                                            <th width="25%">Nama Issue</th>
                                            <th width="25%">Tanggal</th>
                                            <th width="25%">Deskripsi</th>
                                            <th width="100">Aksi</th>
                                          </tr>
                                          </thead>
                                          <tbody id="listViewMom">

                                          </tbody>
                                        </table>                       
                                      </div> 
                                  </div>
                              </div>
                              <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                          </div>          
                        </div>                   
                </div>
            </div>
            <!-- /.box-body -->
          </div>          
        </div>
        <!-- /.col -->
      </div>
	 </section>
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Tambah Data Pekerjaan</h3>
              </div>
              <form action="<?php echo(base_url()) ?>maintenance_product/add" method="post" id="manipulateForm" enctype="multipart/form-data">
                <div class="box-body">
                  <input type="hidden" name="id_detail_kerja" id="id_detail_kerja">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Label Issue</label>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="" required="" autocomplete="off">
                  </div>
                  <div class="form-group">
                      <label for="">Tanggal</label>
                    <input type="text" class="form-control" name="tanggal" id="tanggal" placeholder="" required="" autocomplete="off">
                    <input type="hidden" name="id_kerja" value="<?php echo $id_kerja; ?>" id="id_kerja"> 
                    <input type="hidden" name="id_sub_detail_pekerjaan" id="id_sub_detail_pekerjaan">                   
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Deskripsi</label>
                    <textarea id="deskripsi" name="deskripsi" class="form-control"></textarea>
                  </div>                        
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div id="item" style="display: none;">
                          <div class="col-xs-12">
                            <div class="bottomBorder">
                              <div class="row">
                                  <h5 class="col-md-6" id="leftName">Tahap 1</h5> 
                                    <?php if($status_kerja == "on progress") { ?>
                                    <button id="itemDelete" class="btn btn-danger pull-right" type="button" style="margin-left: 5px; margin-right: 5px;"><i class="fa fa-trash"></i></button>
                                    <button id="itemEdit" class="btn btn-warning pull-right" type="button" style="margin-left: 5px; margin-right: 5px;"><i class="fa fa-pencil"></i></button> 
                                  <?php } else { ?>
                                  <h5 class="col-md-6" id="lblTanggal">Tahap 1</h5> 
                                  <?php } ?>                              
                              </div>

                            </div>                            
                          </div>  
</div>        
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="detailModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
              </div>
              <form action="" method="post" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Issue</label>
                    <input type="text" class="form-control" name="nama" id="detailNama" placeholder="Ketikan Nama" required="" disabled="">
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal</label>
                    <input type="text" class="form-control" name="nama" id="detailTanggal" placeholder="Ketikan Nama" required="" disabled="">
                  </div>                                                                                                                 
                <div class="form-group">
                    <label for="exampleInputPassword1">Deskripsi</label>
                    <div class="custom-form-control" id="detailDeskripsi"></div>
                  </div>                   
                  <div class="form-group">
                    <label>Dibuat Pada </label>
            <input type="text" class="form-control" name="nama" id="detailCreateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Pada </label>
            <input type="text" class="form-control" name="nama" id="detailUpdateAt" placeholder="" required="" disabled="">  
                  </div>                  
                  <input type="hidden" name="access" value="1">
                  <div class="form-group">
                     <label id="editGenerate"></label>                    
                  </div>                                    
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>           